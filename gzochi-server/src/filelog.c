/* filelog.c: A POSIX FILE-based log handler implementation for gzochid
 * Copyright (C) 2020 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <glib/gprintf.h>
#include <glib/gstdio.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include "filelog.h"

struct _gzochid_file_log_context
{
  FILE *log_file;
};

gzochid_file_log_context *
gzochid_file_log_context_new (FILE *log_file)
{
  gzochid_file_log_context *context =
    malloc (sizeof (gzochid_file_log_context));

  context->log_file = log_file;

  return context;
}

void
gzochid_file_log_context_free (gpointer ptr)
{
  gzochid_file_log_context *context = ptr;
  
  fclose (context->log_file);
  free (context);
}

void
gzochid_file_log_handler (const gchar *log_domain, GLogLevelFlags log_level,
			  const gchar *message, gpointer user_data)
{
  gchar *formatted_msg = NULL;
  gzochid_file_log_context *context = user_data;
  GLogField fields[1];

  fields[0].key = "MESSAGE";
  fields[0].value = message;
  fields[0].length = -1;

  formatted_msg = g_log_writer_format_fields (log_level, fields, 1, 0);
  
  g_fprintf (context->log_file, "%s\n", formatted_msg);
  g_free (formatted_msg);
}
