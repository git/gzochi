/* app-store.h: Prototypes and declarations for app-store.c
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GZOCHID_APPLICATION_STORE_H
#define GZOCHID_APPLICATION_STORE_H

#include "gzochid-storage.h"
#include "oids.h"

/* The representation of the storage for a gzochi game application in both the
   application server and the meta server. */

struct _gzochid_application_store
{
  gzochid_storage_engine_interface *iface; /* The storage engine interface. */
  gzochid_storage_context *storage_context; /* The storage context. */

  gzochid_storage_store *oids; /* The object store. */
  gzochid_storage_store *names; /* The binding store. */

  /* The oid allocation strategy. */
  
  gzochid_oid_allocation_strategy *oid_strategy; 
};

typedef struct _gzochid_application_store gzochid_application_store;

/* Allocates and returns a `gzochid_application_store' object, using the 
   specified storage engine interface to initialize a storage and environment
   and open the specified application's object and binding stores. The specified
   `gzochid_oid_allocation_strategy' is bound to the returned object; it should
   not be freed before the application store is freed.

   The resources associated with the store should be released via
   `gzochid_application_store_close' when no longer in use. */

gzochid_application_store *gzochid_application_store_open
(gzochid_storage_engine_interface *, const char *,
 gzochid_oid_allocation_strategy *);

/* Closes and cleans up the resources associated with the specified 
   `gzochid_application_store'. */

void gzochid_application_store_close (gzochid_application_store *);

#endif /* GZOCHID_APPLICATION_STORE_H */
