/* event-app.h: Prototypes and declarations for event-app.c
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GZOCHID_EVENT_APP_H
#define GZOCHID_EVENT_APP_H

#include <glib-object.h>

#include "event.h"

/* Enumeration of meta server event types. */

enum _gzochid_meta_server_event_type
  {
    /* A connection to a meta server has been established. The 
       `connection-description' and (conditionally) the `admin-server-base-url'
       properties will be set. */
    
    META_SERVER_CONNECTED,

    /* The connection to the meta server has been broken. */
    
    META_SERVER_DISCONNECTED 
  };

typedef enum _gzochid_meta_server_event_type gzochid_meta_server_event_type;

/*
  The meta server event sub-type. The following properties are available:

  connection-description: string giving the address of the meta server
  admin-server-base-url: string giving the URL of the meta server's admin web 
    console, if available; `NULL' otherwise
*/

/* The core meta server event type definitions. */

#define GZOCHID_TYPE_META_SERVER_EVENT gzochid_meta_server_event_get_type ()

G_DECLARE_FINAL_TYPE (GzochidMetaServerEvent, gzochid_meta_server_event,
		      GZOCHID, META_SERVER_EVENT, GzochidEvent);

/* Enumeration of durable task event types. */

enum _gzochid_durable_task_event_type
  {
    /* A durable task has been assigned by the meta server. */

    DURABLE_TASK_ASSIGNED, 

    DURABLE_TASK_CANCELED, /* A durable task has been canceled. */    
    DURABLE_TASK_COMPLETED, /* A durable task has been completed. */
    DURABLE_TASK_SUBMITTED, /* A durable task has been submitted. */

    /* A task has been unassigned by the meta server. */

    DURABLE_TASK_UNASSIGNED, 
  };

typedef enum _gzochid_durable_task_event_type gzochid_durable_task_event_type;

/*
  The durable task event sub-type. The following properties are available:

  application: the name of the associated gzochi game application
*/

/* The core durable task event type definitions. */

#define GZOCHID_TYPE_DURABLE_TASK_EVENT gzochid_durable_task_event_get_type ()

G_DECLARE_FINAL_TYPE (GzochidDurableTaskEvent, gzochid_durable_task_event,
		      GZOCHID, DURABLE_TASK_EVENT, GzochidEvent);

#endif /* GZOCHID_EVENT_APP_H */
