/* data.c: Primitive functions for user-facing gzochi data management API
 * Copyright (C) 2020 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <glib.h>
#include <libguile.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "../app.h"
#include "../data.h"
#include "../guile.h"
#include "../reloc.h"
#include "../scheme.h"

#include "data.h"
#include "util.h"

SCM
primitive_create_reference (SCM obj)
{
  GzochidApplicationContext *context =
    gzochid_api_ensure_current_application_context ();
  gzochid_scm_location_info *obj_loc = gzochid_scm_location_get (context, obj);
  gzochid_data_managed_reference *reference = gzochid_data_create_reference 
    (context, &gzochid_scm_location_aware_serialization, obj_loc, NULL);
  SCM ret = SCM_BOOL_F;

  /* The reference returned may be `NULL' if this is a new object and the
     transaction failed to obtain a lock (transaction timeout, e.g.). In that
     case, the call to `gzochid_api_check_transaction' will return 
     non-locally. */
  
  if (reference != NULL)
    {  
      scm_gc_protect_object (obj);
      ret = gzochid_scheme_create_managed_reference (reference);
    }
   
  gzochid_api_check_transaction ();
  return ret;
}

SCM
primitive_dereference (SCM ref)
{
  GError *err = NULL;
  GzochidApplicationContext *context =
    gzochid_api_ensure_current_application_context ();
  gzochid_data_managed_reference *reference = NULL;
  SCM ret = SCM_BOOL_F;

  guint64 oid = gzochid_scheme_managed_reference_oid (ref);

  reference = gzochid_data_create_reference_to_oid 
    (context, &gzochid_scm_location_aware_serialization, oid);

  gzochid_data_dereference (reference, &err);

  if (err == NULL)
    {
      if (reference->obj == NULL
	  || reference->state == 
	  GZOCHID_MANAGED_REFERENCE_STATE_REMOVED_FETCHED)
	gzochid_guile_r6rs_raise 
	  (gzochid_scheme_make_object_removed_condition ());
      else 
	{
	  gzochid_scm_location_info *location = 
	    (gzochid_scm_location_info *) reference->obj;
	  ret = gzochid_scm_location_resolve (context, location);
	}
    }
  else gzochid_api_check_not_found (err);

  gzochid_api_check_transaction ();

  return ret;
}

static char *prefix_name (char *name)
{
  int name_len = strlen (name);
  char *prefixed_name = malloc (sizeof (char) * (name_len + 3));

  memcpy (prefixed_name, "o.", 3);
  memcpy (prefixed_name + 2, name, name_len + 1);

  return prefixed_name;
}

SCM
primitive_get_binding (SCM name)
{
  GError *err = NULL;
  GzochidApplicationContext *context =
    gzochid_api_ensure_current_application_context ();
  
  char *cname = scm_to_locale_string (name);
  char *prefixed_name = prefix_name (cname);
  
  SCM ret = SCM_BOOL_F;
  SCM cond = SCM_BOOL_F;
  
  if (! gzochid_data_binding_exists (context, prefixed_name, &err))
    {
      if (err == NULL)	
	cond = gzochid_scheme_make_name_not_bound_condition (cname);
      else g_error_free (err);

      free (cname);
      free (prefixed_name);
    }
  else 
    {
      gzochid_scm_location_info *location = 
	(gzochid_scm_location_info *) gzochid_data_get_binding 
	(context, prefixed_name, &gzochid_scm_location_aware_serialization, 
	 &err);

      free (cname);
      free (prefixed_name);

      if (err == NULL)
	{
	  if (location != NULL)
	    ret = gzochid_scm_location_resolve (context, location);
	}
      else gzochid_api_check_not_found (err);
    }

  gzochid_api_check_transaction ();

  if (! scm_is_false (cond))    
    gzochid_guile_r6rs_raise (cond);

  return ret;
}

SCM
primitive_set_binding_x (SCM name, SCM obj)
{
  GError *err = NULL;
  GzochidApplicationContext *context =
    gzochid_api_ensure_current_application_context ();
  char *cname = scm_to_locale_string (name);
  char *prefixed_name = prefix_name (cname);
  
  if (gzochid_data_binding_exists (context, prefixed_name, &err))
    {
      SCM cond = gzochid_scheme_make_name_exists_condition (cname);

      free (cname);
      free (prefixed_name);

      gzochid_guile_r6rs_raise (cond);
    }
  else if (err == NULL)
    {
      gzochid_scm_location_info *obj_loc = 
	gzochid_scm_location_get (context, obj);

      scm_gc_protect_object (obj);
      
      gzochid_data_set_binding 
	(context, prefixed_name, &gzochid_scm_location_aware_serialization, 
	 obj_loc, NULL);
      
      free (cname);
      free (prefixed_name);
    }

  gzochid_api_check_transaction ();
  return SCM_UNSPECIFIED;
}

SCM
primitive_remove_binding_x (SCM name)
{
  GError *err = NULL;
  GzochidApplicationContext *context =
    gzochid_api_ensure_current_application_context ();
  char *cname = scm_to_locale_string (name);
  char *prefixed_name = prefix_name (cname);
  
  gzochid_data_remove_binding (context, prefixed_name, &err);

  free (prefixed_name);

  if (err != NULL)
    {
      if (g_error_matches (err, GZOCHID_DATA_ERROR, GZOCHID_DATA_ERROR_FAILED))
	{
	  SCM cond = gzochid_scheme_make_name_not_bound_condition (cname);
	  
	  free (cname);
	  g_error_free (err);
      
	  gzochid_guile_r6rs_raise (cond);
	}
      else g_error_free (err);
    }

  free (cname);

  gzochid_api_check_transaction ();
  return SCM_UNSPECIFIED;
}

SCM
primitive_remove_object_x (SCM obj)
{
  GzochidApplicationContext *context =
    gzochid_api_ensure_current_application_context ();
  gzochid_scm_location_info *obj_loc = gzochid_scm_location_get (context, obj);
  gzochid_data_managed_reference *reference = gzochid_data_create_reference 
    (context, &gzochid_scm_location_aware_serialization, obj_loc, NULL);

  /*
    The reference returned may be `NULL' if this is a new object and the
    transaction failed to obtain a lock (transaction timeout, e.g.). 

    ...But when used properly, this function shouldn't be called with new 
    objects, only those that were previously dereferenced. So there's little
    risk of transaction failure, but `gzochid_api_check_transaction' will handle
    it if necessary. 
  */

  if (reference != NULL)
    {
      scm_gc_protect_object (obj);    
      gzochid_data_remove_object (reference, NULL);
    }
  
  gzochid_api_check_transaction ();
  return SCM_UNSPECIFIED;
}

SCM
primitive_mark_for_write_x (SCM obj)
{
  GzochidApplicationContext *context =
    gzochid_api_ensure_current_application_context ();
  gzochid_scm_location_info *obj_loc = gzochid_scm_location_get (context, obj);

  scm_gc_protect_object (obj);    

  gzochid_data_mark 
    (context, &gzochid_scm_location_aware_serialization, obj_loc, NULL);

  gzochid_api_check_transaction ();

  return SCM_UNSPECIFIED;
}

void gzochid_api_data_init (void)
{
  scm_call_1
    (scm_c_public_ref
     ("gzochi private data", "gzochi:set-primitive-create-reference!"),
     scm_c_make_gsubr ("primitive-create-reference", 1, 0, 0,
		       primitive_create_reference));
  scm_call_1
    (scm_c_public_ref
     ("gzochi private data", "gzochi:set-primitive-dereference!"),
     scm_c_make_gsubr ("primitive-dereference", 1, 0, 0,
		       primitive_dereference));
  scm_call_1
    (scm_c_public_ref
     ("gzochi private data", "gzochi:set-primitive-get-binding!"),
     scm_c_make_gsubr ("primitive-get-binding", 1, 0, 0,
		       primitive_get_binding));
  scm_call_1
    (scm_c_public_ref
     ("gzochi private data", "gzochi:set-primitive-set-binding!"),
     scm_c_make_gsubr ("primitive-set-binding!", 2, 0, 0,
		       primitive_set_binding_x));
  scm_call_1
    (scm_c_public_ref
     ("gzochi private data", "gzochi:set-primitive-remove-binding!"),
     scm_c_make_gsubr ("primitive-remove-binding!", 1, 0, 0,
		       primitive_remove_binding_x));
  scm_call_1
    (scm_c_public_ref
     ("gzochi private data", "gzochi:set-primitive-remove-object!"),
     scm_c_make_gsubr ("primitive-remove-object!", 1, 0, 0,
		       primitive_remove_object_x));
  scm_call_1
    (scm_c_public_ref
     ("gzochi private data", "gzochi:set-primitive-mark-for-write!"),
     scm_c_make_gsubr ("primitive-mark-for-write!", 1, 0, 0,
		       primitive_mark_for_write_x));
}
