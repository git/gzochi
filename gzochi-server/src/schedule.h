/* schedule.h: Prototypes and declarations for schedule.c
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GZOCHID_SCHEDULE_H
#define GZOCHID_SCHEDULE_H

#include <glib.h>
#include <glib-object.h>

#include "task.h"

/* The core task queue type definitions. */

#define GZOCHID_TYPE_TASK_QUEUE gzochid_task_queue_get_type ()

G_DECLARE_FINAL_TYPE (GzochidTaskQueue, gzochid_task_queue, GZOCHID, TASK_QUEUE,
		      GObject);

gpointer gzochid_schedule_task_executor (gpointer);

void gzochid_schedule_task_queue_start (GzochidTaskQueue *);

/* Stops the thread feeding tasks from the specified task queue. Once this
   function returns, no additional tasks will be drained from the queue to its
   associated `GThreadPool', though any tasks that have already been drained for
   immediate execution will still execute. */

void gzochid_schedule_task_queue_stop (GzochidTaskQueue *);

/*
  Submits the specified task for execution in the specified task queue and 
  returns immediately. The task will be executed no earlier than its configured
  execution time.
  
  Returns a locally-unique task id that can be used to remove the task from the
  task queue via `gzochid_schedule_remove_task'. 
*/

guint64 gzochid_schedule_submit_task (GzochidTaskQueue *, GzochidTask *);

/* Attempts to remove the task associated with the specified locally-unique id 
   from the local task queue so that it does not execute. Returns `TRUE' if the
   task was successfully removed, `FALSE' if the task could not be found
   (because it is currently executing or has already finished executing). */

gboolean gzochid_schedule_remove_task (GzochidTaskQueue *, guint64);

/* Submits the specified list of tasks for execution in the specified task 
   queue and returns immediately. Each task is submitted in the order specified
   in the queue once the previous task has executed; each task will be executed
   no earlier than its configured execution time. 

   The task execution code operates on a deep copy of the specified `GList'. */

void gzochid_schedule_submit_task_chain (GzochidTaskQueue *, GList *);

/* Submits the specified tasks for execution in the specified task queue and 
   waits until it has been executed before returning. The task will be executed
   no earlier than its configured execution time. */

void gzochid_schedule_run_task (GzochidTaskQueue *, GzochidTask *);

/* Synchronously executes the specified task in the calling thread. */

void gzochid_schedule_execute_task (GzochidTask *);

#endif /* GZOCHID_SCHEDULE_H */
