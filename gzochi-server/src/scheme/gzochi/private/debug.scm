;; gzochi/private/debug.scm: Remote debugging interface for gzochid
;; Copyright (C) 2019 Julian Graham
;;
;; gzochi is free software: you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!r6rs

(library (gzochi private debug)
  (export gzochi:run-repl-server gzochi:stop-repl-server)
  (import (guile)
	  (ice-9 match)
	  (ice-9 optargs)
	  (ice-9 threads)
	  (rnrs base)
	  (rnrs eval)
	  (rnrs io simple)
	  (system repl repl))

  ;; The vast majority of the following code is borrowed from the 
  ;; `(system repl server)' module in GNU Guile.

  (define *open-sockets* '())
  (define sockets-lock (make-mutex))

  (define (close-socket! s)
    (with-mutex sockets-lock
      (set! *open-sockets* (assq-remove! *open-sockets* s)))
    (close-port s))

  (define (add-open-socket! s force-close)
    (with-mutex sockets-lock
      (set! *open-sockets* (acons s force-close *open-sockets*))))
  
  (define (stop-server-and-clients!)
    (cond
     ((with-mutex sockets-lock
        (match *open-sockets*
	       (() #f)
	       (((s . force-close) . rest)
		(set! *open-sockets* rest)
		force-close)))
      => (lambda (force-close)
	   (force-close)
	   (stop-server-and-clients!)))))

  (define* (make-tcp-server-socket
	    #:key (host #f)
	          (addr (if host (inet-pton AF_INET host) INADDR_LOOPBACK))
		  (port 37146))
    (let ((sock (socket PF_INET SOCK_STREAM 0)))
      (setsockopt sock SOL_SOCKET SO_REUSEADDR 1)
      (bind sock AF_INET addr port)
      sock))

  (define* (run-server #:optional (server-socket (make-tcp-server-socket)))
    (run-server* server-socket serve-client))

  (define (run-server* server-socket serve-client)
    (define shutdown-pipes (pipe))
    (define shutdown-read-pipe (car shutdown-pipes))
    (define shutdown-write-pipe (cdr shutdown-pipes))
    
    (define (shutdown-server)
      (display #\! shutdown-write-pipe)
      (force-output shutdown-write-pipe))
    
    (define monitored-ports
      (list server-socket shutdown-read-pipe))

    (define (accept-new-client)
      (let ((ready-ports (car (select monitored-ports '() '()))))
	(and (not (memq shutdown-read-pipe ready-ports))
	     (or (false-if-exception (accept server-socket)
				     #:warning "Failed to accept client:")
		 (accept-new-client)))))

    (fcntl server-socket
	   F_SETFL (logior O_NONBLOCK (fcntl server-socket F_GETFL)))

    (sigaction SIGPIPE SIG_IGN)
    (add-open-socket! server-socket shutdown-server)
    (listen server-socket 5)
    (let lp ()
      (match (accept-new-client)
	     (#f
	      (close shutdown-write-pipe)
	      (close shutdown-read-pipe)
	      (close server-socket))
	     ((client-socket . client-addr)
	      (make-thread serve-client client-socket client-addr)
	      (lp)))))
 
  (define (serve-client client addr)

    ;; Create the "sandbox" environment for connecting debugger users. It's not
    ;; really a true sandbox per se -- more of a private module environment 
    ;; with some helpful modules pre-included.
    
    (define client-environment
      (environment '(guile) '(gzochi) '(gzochi admin) '(rnrs)))
    
    (let ((thread (current-thread)))
      (add-open-socket! client (lambda () (cancel-thread thread))))
    
    (dynamic-wind
	(lambda () #f)
	(with-continuation-barrier
	 (lambda ()
	   (parameterize ((current-input-port client)
			  (current-output-port client)
			  (current-error-port client)
			  (current-warning-port client))
			 (with-fluids ((*repl-stack* '()))
			   (set-current-module client-environment)
			   (start-repl)))))
	(lambda () (close-socket! client))))  

  (define* (gzochi:run-repl-server #:key (port 37146))
    (run-server (make-tcp-server-socket #:port port)))

  (define gzochi:stop-repl-server stop-server-and-clients!)
)
