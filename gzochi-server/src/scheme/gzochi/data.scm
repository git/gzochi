;; gzochi/data.scm: Public exports for gzochi data API
;; Copyright (C) 2018 Julian Graham
;;
;; gzochi is free software: you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!r6rs

(library (gzochi data)

  (export gzochi:make-managed-record-type-descriptor
	  gzochi:make-managed-record-constructor-descriptor
	  gzochi:managed-record-accessor
	  gzochi:managed-record-mutator
	  gzochi:managed-record-constructor
	  gzochi:managed-record-predicate

          gzochi:make-managed-record-type-registry
          gzochi:managed-record-type-registry?

	  gzochi:managed-record?
	  gzochi:managed-record-rtd
	  gzochi:managed-record-type-name
	  gzochi:managed-record-type-parent
	  gzochi:managed-record-type-uid

          gzochi:define-managed-record-type
	  gzochi:managed-record-type-descriptor
	  gzochi:managed-record-constructor-descriptor

	  gzochi:car
	  gzochi:cdr
	  gzochi:make-managed-pair
	  gzochi:managed-pair?
	  gzochi:cons

	  gzochi:managed-list
	  gzochi:managed-list->list
	  gzochi:list->managed-list

	  gzochi:make-managed-serializable
	  gzochi:managed-serializable?
	  gzochi:managed-serializable-value
	  gzochi:managed-serializable-value-set!
	  
	  gzochi:make-managed-vector
	  gzochi:managed-vector
	  gzochi:managed-vector?
	  gzochi:managed-vector-ref
	  gzochi:managed-vector-set!
	  gzochi:managed-vector-length
	  gzochi:managed-vector->list

	  gzochi:make-managed-sequence
	  gzochi:managed-sequence?
	  gzochi:managed-sequence->list
	  gzochi:managed-sequence-add!
	  gzochi:managed-sequence-contains?
	  gzochi:managed-sequence-delete!
	  gzochi:managed-sequence-delete-at!
	  gzochi:managed-sequence-fold-left
	  gzochi:managed-sequence-fold-right
	  gzochi:managed-sequence-insert!
	  gzochi:managed-sequence-ref
	  gzochi:managed-sequence-size

	  gzochi:make-managed-hashtable
	  gzochi:managed-hashtable?
	  gzochi:managed-hashtable-size
	  gzochi:managed-hashtable-ref
	  gzochi:managed-hashtable-set!
	  gzochi:managed-hashtable-delete!
	  gzochi:managed-hashtable-contains?
	  gzochi:managed-hashtable-update!
	  gzochi:managed-hashtable-clear!
	  gzochi:managed-hashtable-keys
	  gzochi:managed-hashtable-entries
	  gzochi:managed-hashtable-hash-function
	  gzochi:managed-hashtable-equivalence-function

	  gzochi:serialize
	  gzochi:deserialize

	  gzochi:create-reference
	  gzochi:dereference
	  gzochi:get-binding
	  gzochi:set-binding!
	  gzochi:remove-binding!
	  gzochi:remove-object!)

  (import (gzochi io)
	  (gzochi private app)
	  (gzochi private data)
	  (gzochi private reflect)
	  (ice-9 optargs)
	  (rnrs)
	  (only (guile) keyword? modulo)
	  (only (srfi :1) split-at)
	  (srfi :2)
	  (srfi :8))

  (gzochi:define-managed-record-type 
    (gzochi:managed-pair gzochi:make-managed-pair gzochi:managed-pair?)

    (fields (immutable ca gzochi:managed-pair-car)
	    (immutable cd gzochi:managed-pair-cdr))
    (sealed #t))

  (define gzochi:car gzochi:managed-pair-car)
  (define gzochi:cdr gzochi:managed-pair-cdr)
  (define (gzochi:cons x y)
    (or (and (or (gzochi:managed-record? x) (not x))
	     (or (gzochi:managed-record? y) (not y) (null? y)))
	(assertion-violation 'gzochi:cons "Arguments must be managed records."))
    (gzochi:make-managed-pair x (if (null? y) #f y)))

  (define (gzochi:managed-list h . t)
    (gzochi:cons h (if (null? t) t (gzochi:managed-list (car t) (cdr t)))))

  (define (gzochi:managed-list->list l)
    (if l (cons (gzochi:car l) (gzochi:managed-list->list (gzochi:cdr l))) '()))
  
  (define (gzochi:list->managed-list l)
    (and (not (null? l))
	 (gzochi:cons (car l) (gzochi:list->managed-list (cdr l)))))

  ;; Convenience serialization instance for a list of symbols (i.e., a module
  ;; name)
  
  (define module-name-serialization
    (gzochi:make-uniform-list-serialization gzochi:symbol-serialization))

  ;; Serializes a pair whose `car' is a gzochi:serialization-reference to the
  ;; specified port, then uses the serializer to serialize the `cdr' of that
  ;; pair.
  
  (define (serialize-serialization-with-value port pair)
    (define (write-module-procedure module procedure)
      ((gzochi:serialization-serializer module-name-serialization) port module)
      (gzochi:write-symbol port procedure))

    (let* ((serialization (car pair))
	   (serializer (gzochi:serialization-serializer serialization)))
      (write-module-procedure
       (gzochi:serialization-reference-serializer-module-name serialization)
       (gzochi:serialization-reference-serializer-procedure serialization))
      (write-module-procedure
       (gzochi:serialization-reference-deserializer-module-name serialization)
       (gzochi:serialization-reference-deserializer-procedure serialization))
      (serializer port (cdr pair))))

  ;; Deserializes a gzochi:serialization-reference from the specified port, then
  ;; uses the deserializer to deserialize a value from the port. Returns a pair
  ;; of the serialization reference and the value.
  
  (define (deserialize-serialization-with-value port)
    (define (read-module)
      ((gzochi:serialization-deserializer module-name-serialization) port))

    (let* ((serializer-module-name (read-module))
	   (serializer-procedure (gzochi:read-symbol port))
	   (deserializer-module-name (read-module))
	   (deserializer-procedure (gzochi:read-symbol port))
	   (serialization (gzochi:make-serialization-reference
			   serializer-procedure serializer-module-name
			   deserializer-procedure deserializer-module-name))
	   (deserializer (gzochi:serialization-deserializer serialization)))
      (cons serialization (deserializer port))))

  ;; A serialization instance for serialization-with-value pairs.
  
  (define serialization-with-value-serialization
    (gzochi:make-serialization 
     serialize-serialization-with-value deserialize-serialization-with-value))

  ;; A managed record type that wraps an arbitrary (unmanaged) value along with
  ;; a gzochi:serialization-reference.
  
  (gzochi:define-managed-record-type 
   (gzochi:managed-serializable 
    gzochi:make-managed-serializable 
    gzochi:managed-serializable?)
   (fields (mutable serialization-with-value 
		    (serialization serialization-with-value-serialization)))
   (protocol (lambda (p)
	       (lambda (value serialization)
		 (or (gzochi:serialization-reference? serialization)
		     (assertion-violation
		      'gzochi:make-managed-serializable
		      "Expected serialization reference." serialization))
		 (p (cons serialization value))))))

  ;; Accessor for the serialization component of a gzochi:managed-serializable
  ;; object.
  
  (define (gzochi:managed-serializable-serialization obj)
    (car (gzochi:managed-serializable-serialization-with-value obj)))

  ;; Accessor for the value component of a gzochi:managed-serializable object.

  (define (gzochi:managed-serializable-value obj)
    (cdr (gzochi:managed-serializable-serialization-with-value obj)))

  ;; Mutator for the value component of a gzochi:managed-serializable object.
  
  (define (gzochi:managed-serializable-value-set! obj val)
    (let ((l (gzochi:managed-serializable-serialization-with-value obj)))
      (gzochi:managed-serializable-serialization-with-value-set!
       obj (cons (car l) val))
      (if #f #f)))

  ;; Serializer for the inner vector of gzochi:managed-vector. Writes the length
  ;; of the vector followed by its (sparse) series of managed references.
  
  (define (serialize-managed-vector port vec)
    (gzochi:write-integer port (vector-length vec))
    (vector-for-each
     (lambda (ref) (gzochi:serialize-managed-reference port ref)) vec))

  ;; Deserializer for the inner vector of gzochi:managed-vector. Reads the
  ;; length of the vector followed by a sparse series of managed references.
  
  (define (deserialize-managed-vector port)
    (let* ((n (gzochi:read-integer port)))
      (let loop ((i n) (refs '()))
	(if (zero? i)
	    (list->vector (reverse refs))
	    (loop (- i 1) 
		  (cons (gzochi:deserialize-managed-reference port) refs))))))

  (gzochi:define-managed-record-type
   (managed-vector gzochi:make-managed-vector gzochi:managed-vector?)

   (fields ;; A bit field indicating which values in the vector have been
           ;; implicitly converted to gzochi:managed-serializables and should
           ;; thus be unwrapped on access.

           (mutable wrap-bit-set
		    gzochi:managed-vector-wrap-bit-set
		    gzochi:set-managed-vector-wrap-bit-set!
		    (serialization gzochi:integer-serialization))

	   ;; The inner vector for managed reference storage.
	   
           (immutable vector
		      gzochi:managed-vector-vector
		      (serialization (gzochi:make-serialization 
				      serialize-managed-vector
				      deserialize-managed-vector))))

   (protocol (lambda (p) (lambda (l) (p 0 (make-vector l #f)))))
   (sealed #t))

  (define (gzochi:managed-vector-length vec)
    (vector-length (gzochi:managed-vector-vector vec)))

  ;; Conditionally wrap the specified object as a gzochi:managed-serializable,
  ;; or just return it if it's already a managed record.
  
  (define (maybe-wrap obj serialization)
    (if (gzochi:managed-record? obj)
	obj
	(if (gzochi:serialization-reference? serialization)
	    (gzochi:make-managed-serializable obj serialization)
	    (raise (assertion-violation
		    'gzochi:make-managed-serializable
		    "Serialization must be specified for unmanaged values.")))))

  ;; Retrieves the i'th value from the specified managed vector. If the value
  ;; was originally unmanaged, "unwrap" it before returning it.
  
  (define (gzochi:managed-vector-ref vec i)
    (let ((obj (vector-ref (gzochi:managed-vector-vector vec) i)))
      (and obj
	   (let ((obj (gzochi:dereference obj)))
	     (if (bitwise-bit-set? (gzochi:managed-vector-wrap-bit-set vec) i)
		 (gzochi:managed-serializable-value obj)
		 obj)))))

  ;; Returns a list of the objects in the specified managed vector.
  
  (define (gzochi:managed-vector->list vec)
    (let ((wrap-bit-set (gzochi:managed-vector-wrap-bit-set vec)))
      (let loop ((i 0)
		 (wrapped-lst (vector->list (gzochi:managed-vector-vector vec)))
		 (unwrapped-lst '()))
	(if (null? wrapped-lst)
	    (reverse unwrapped-lst)
	    (let ((obj (gzochi:dereference (car wrapped-lst))))
	      (loop (+ i 1)
		    (cdr wrapped-lst)
		    (cons (if (bitwise-bit-set? wrap-bit-set i)
			      (gzochi:managed-serializable-value obj)
			      obj)
			  unwrapped-lst)))))))

  ;; Inserts the specified object into the specified managed vector at the
  ;; specified index. Unmanaged objects will be automatically wrapped as a
  ;; gzochi:managed-serializable object.
  
  (define* (gzochi:managed-vector-set! vec i obj #:key serialization)
    (let* ((unmanaged-vec (gzochi:managed-vector-vector vec))
	   (ref (vector-ref unmanaged-vec i))
	   (entry (and ref (gzochi:dereference ref)))
	   (wrap-bit-set (gzochi:managed-vector-wrap-bit-set vec)))

      (if (and entry (bitwise-bit-set? wrap-bit-set i))
	  (gzochi:remove-object! entry))
      
      (if obj
	  (let* ((wrapped-obj (maybe-wrap obj serialization)))
	    (vector-set! unmanaged-vec i (gzochi:create-reference wrapped-obj))
	    (gzochi:set-managed-vector-wrap-bit-set!
	     vec (bitwise-copy-bit wrap-bit-set i
				   (if (eq? wrapped-obj obj) 0 1))))
	  (begin
	    (vector-set! unmanaged-vec i #f)
	    (gzochi:set-managed-vector-wrap-bit-set!
	     vec (bitwise-copy-bit wrap-bit-set i 0)))))
  
    (gzochi:mark-for-write! vec))

  ;; Alternate constructor for gzochi:managed-vector. Allows managed vectors to
  ;; be constructed from a literal sequence of values. Unmanaged values in this
  ;; sequence will be wrapped as managed serializable objects using the
  ;; specified serialization.
  
  (define* (gzochi:managed-vector #:key serialization #:rest l)
    (define constructor
      (gzochi:managed-record-constructor
       (gzochi:make-managed-record-constructor-descriptor
	managed-vector #f (lambda (p)
			    (lambda (wrap-bits vec) (p wrap-bits vec))))))

    (let loop ((i 0)
	       (lst l)
	       (objs '())
	       (wrap-bits 0)
	       (last-was-keyword? #f))
      
      (cond ((null? lst) (constructor (bitwise-reverse-bit-field wrap-bits 0 i)
				      (list->vector (reverse objs))))
	    (last-was-keyword? (loop i (cdr lst) objs wrap-bits #f))
	    (else (let ((obj (car lst)))
		    (if (keyword? obj)
			(loop i (cdr lst) objs wrap-bits #t)
			(let ((wobj (maybe-wrap obj serialization)))
			  (loop (+ i 1)
				(cdr lst)
				(cons (gzochi:create-reference wobj) objs)
				(bitwise-ior
				 (bitwise-arithmetic-shift wrap-bits 1)
				 (if (eq? obj wobj) 0 1))
				#f))))))))

  ;; Destructively shifts the contents of the specified managed vector right by
  ;; the specified count (which must be 1) starting at the specified index. This
  ;; procedure effectively makes a space for a new entry at the specified index
  ;; within the vector.
  
  (define (managed-vector-shift-right! mvec i count)
    (or (eqv? count 1) 
	(assertion-violation
	 'managed-vector-shift-right! "Cannot shift by more than 1 position"))
    
    (let* ((vec (gzochi:managed-vector-vector mvec))
	   (wrap-bit-set (gzochi:managed-vector-wrap-bit-set mvec))
	   (len (vector-length vec)))

      ;; Shift the "upper" region of the wrap bit set left by one, then copy the
      ;; original "lower" region back into place. It's a left shift because the
      ;; bit field counts indexes left starting at the right.
      
      (gzochi:set-managed-vector-wrap-bit-set!
       mvec (bitwise-ior (bitwise-arithmetic-shift
			  (bitwise-bit-field wrap-bit-set i len) (+ i 1))
			 (bitwise-bit-field wrap-bit-set 0 i)))
      
      (let loop ((j (- len 1)))
	(if (eqv? i j)
	    (vector-set! vec i #f)
	    (let ((jj (- j 1)))
	      (vector-set! vec j (vector-ref vec jj))
	      (loop jj))))

      (gzochi:mark-for-write! mvec)))

  ;; Destructively shifts the contents of the specified managed vector left by
  ;; the specified count (which must be 1) starting at the specified index. This
  ;; procedure effectively makes a space for a new entry at the specified index
  ;; within the vector.
  
  (define (managed-vector-shift-left! mvec i count)
    (or (eqv? count 1)
	(assertion-violation
	 'managed-vector-shift-left! "Cannot shift by more than 1 position"))

    (let* ((vec (gzochi:managed-vector-vector mvec))
	   (wrap-bit-set (gzochi:managed-vector-wrap-bit-set mvec))
	   (len (vector-length vec))
	   (j (- len 1)))

      ;; Shift the "lower" region of the wrap bit set right by one, then copy
      ;; the original "upper" region back into place. It's a right shift because
      ;; the bit field counts indexes left starting at the right.
      
      (gzochi:set-managed-vector-wrap-bit-set!
       mvec (bitwise-ior (bitwise-arithmetic-shift-right
			  (bitwise-bit-field wrap-bit-set 0 i) 1)
			 (bitwise-arithmetic-shift
			  (bitwise-bit-field wrap-bit-set i len) i)))
      
      (let loop ((i i))
	(if (eqv? i j)
	    (vector-set! vec i #f)
	    (let ((ii (+ i 1)))
	      (vector-set! vec i (vector-ref vec ii))
	      (loop ii))))

      (gzochi:mark-for-write! mvec)))

  ;; Destructively moves the range of contents from the specified source managed
  ;; vector to the specified destination vector, replacing the source values
  ;; with `#f'.
  
  (define (managed-vector-transfer! dst dst-from src src-from src-to)
    (define (vector-transfer! dst-vec dst-idx src-vec src-idx)      
      (vector-set! dst-vec dst-idx (vector-ref src-vec src-idx))
      (vector-set! src-vec src-idx #f))
    
    (let ((dst-vec (gzochi:managed-vector-vector dst))
	  (dst-wrap-bit-set (gzochi:managed-vector-wrap-bit-set dst))
	  (src-vec (gzochi:managed-vector-vector src))
	  (src-wrap-bit-set (gzochi:managed-vector-wrap-bit-set src)))

      (gzochi:set-managed-vector-wrap-bit-set!
       dst (bitwise-copy-bit-field
	    dst-wrap-bit-set src-from src-to src-wrap-bit-set))
      (gzochi:set-managed-vector-wrap-bit-set!
       src (bitwise-copy-bit-field src-wrap-bit-set src-from src-to 0))
      
      (let loop ((src-from src-from)
		 (dst-from dst-from))
	(if (< src-from src-to)
	    (begin
	      (vector-transfer! dst-vec dst-from src-vec src-from)
	      (loop (+ src-from 1) (+ dst-from 1)))
	    (begin
	      (gzochi:mark-for-write! dst)
	      (gzochi:mark-for-write! src))))))

  (gzochi:define-managed-record-type managed-sequence-subsequence
    (fields contents
	    (immutable max-size (serialization gzochi:integer-serialization))
	    (mutable size (serialization gzochi:integer-serialization)))
    (protocol (lambda (p)
		(lambda (max-size)
		  (p (gzochi:make-managed-vector (+ max-size 1)) 
		     max-size 
		     0)))))

  ;; Inserts the specified value at the specified index in the specified
  ;; managed subsequence. The specified serialization is used to wrap the value
  ;; as a managed serializable if necessary.
  
  (define (managed-sequence-subsequence-insert! subseq i obj serialization) 
    (let ((content (managed-sequence-subsequence-contents subseq))
	  (size (managed-sequence-subsequence-size subseq)))

      (or (<= i (managed-sequence-subsequence-max-size subseq))
	  (assertion-violation
	   'managed-sequence-subsequence-insert! "Index out of bounds." i))

      (managed-vector-shift-right! content i 1)
      (gzochi:managed-vector-set! content i obj #:serialization serialization)
      (managed-sequence-subsequence-size-set! subseq (+ size 1))))

  (define (managed-sequence-subsequence-delete-at! subseq i)
    (let ((content (managed-sequence-subsequence-contents subseq))
	  (size (managed-sequence-subsequence-size subseq)))

      (managed-vector-shift-left! content i 1)
      (managed-sequence-subsequence-size-set! subseq (- size 1))))

  (define (managed-sequence-subsequence-delete! subseq obj pred)
    (let ((content (managed-sequence-subsequence-contents subseq))
	  (size (managed-sequence-subsequence-size subseq)))
      
      (let loop ((i 0))
	(and (< i size)
	     (let ((val (gzochi:managed-vector-ref content i))
		   (pred (or pred eq?)))
	       (if (pred val obj) 
		   (managed-sequence-subsequence-delete-at! subseq i)
		   (loop (+ i 1))))))))

  (gzochi:define-managed-record-type managed-sequence-node
    (fields (mutable parent) 
	    (mutable next) 
	    (mutable prev)
	    (mutable size (serialization gzochi:integer-serialization))))

  (gzochi:define-managed-record-type managed-sequence-list-node
    (fields subsequence)
    (parent managed-sequence-node)
    (protocol (lambda (n) 
		(lambda (parent max-size) 
		  (let ((p (n parent #f #f 0)))
		    (p (make-managed-sequence-subsequence max-size)))))))

  ;; Inserts the specified value at the specified index in the subsequence
  ;; attached to the specified managed sequence list node, splitting the node
  ;; afterward if necessary. The specified serialization is used to wrap the
  ;; value as a managed serializable if necessary.
  
  (define (managed-sequence-list-node-insert! node i obj serialization)
    (let ((subseq (managed-sequence-list-node-subsequence node))
	  (count (managed-sequence-node-size node)))      
      (managed-sequence-subsequence-insert! subseq i obj serialization)
      (managed-sequence-node-size-set! node (+ count 1))
      (if (>= count (managed-sequence-subsequence-max-size subseq))
	  (managed-sequence-list-node-split! node)
	  (managed-sequence-tree-node-increment! 
	   (managed-sequence-node-parent node)))))

  (define (managed-sequence-list-node-decrement! node)
    (let ((subseq (managed-sequence-list-node-subsequence node))
	  (count (managed-sequence-node-size node)))
      (managed-sequence-node-size-set! node (- count 1))
      (if (eqv? count 1)
	  (let ((next (managed-sequence-node-next node))
		(prev (managed-sequence-node-prev node)))
	    (if next
		(if prev
		    (begin 
		      (managed-sequence-node-next-set! prev next)
		      (managed-sequence-node-prev-set! next prev))
		    (managed-sequence-node-prev-set! next #f))
		(if prev (managed-sequence-node-next-set! prev #f)))
	    (if (or next prev)
		(begin
		  (gzochi:remove-object! node)
		  (gzochi:remove-object! subseq))))
	  (managed-sequence-tree-node-decrement! 
	   (managed-sequence-node-parent node)))))

  (define (managed-sequence-list-node-delete! node obj pred)
    (let ((subseq (managed-sequence-list-node-subsequence node)))
      (and (managed-sequence-subsequence-delete! subseq obj pred)
	   (begin (managed-sequence-list-node-decrement! node) #t))))

  (define (managed-sequence-list-node-delete-at! node i)
    (let ((subseq (managed-sequence-list-node-subsequence node)))
      (managed-sequence-subsequence-delete-at! subseq i)
      (managed-sequence-list-node-decrement! node)))

  (define (managed-sequence-list-node-split! node)
    (let* ((subseq (managed-sequence-list-node-subsequence node))
	   (contents (managed-sequence-subsequence-contents subseq))
	   (node2 (make-managed-sequence-list-node 
		   (managed-sequence-node-parent node) 10))
	   (subseq2 (managed-sequence-list-node-subsequence node2))
	   (contents2 (managed-sequence-subsequence-contents subseq2))
	   (size (managed-sequence-subsequence-size subseq))
	   (half (div size 2))
	   (new-size (- size half)))
	  
      (managed-vector-transfer! contents2 0 contents half size)

      (managed-sequence-node-size-set! node half)
      (managed-sequence-subsequence-size-set! subseq half)
      (managed-sequence-node-size-set! node2 new-size)
      (managed-sequence-subsequence-size-set! subseq2 new-size)
      
      (managed-sequence-node-next-set! node2 (managed-sequence-node-next node))
      (managed-sequence-node-prev-set! node2 node)
      (and-let* ((next (managed-sequence-node-next node)))
	(managed-sequence-node-prev-set! next node2))
      (managed-sequence-node-next-set! node node2)

      (managed-sequence-tree-node-increment! 
       (managed-sequence-node-parent node) #t)))

  ;; Appends the specified value to the end of the specified managed sequence
  ;; list node. The specified serialization is used to wrap the value as a
  ;; managed serializable if necessary.
  
  (define (managed-sequence-list-node-append! node obj serialization)
    (managed-sequence-list-node-insert!
     node (managed-sequence-node-size node) obj serialization))

  ;; Prepends the specified value to the beginning of the specified managed
  ;; sequence list node. The specified serialization is used to wrap the value
  ;; as a managed serializable if necessary.

  (define (managed-sequence-list-node-prepend! node obj serialization)
    (managed-sequence-list-node-insert! node 0 obj serialization))

  (gzochi:define-managed-record-type managed-sequence-tree-node 
    (fields owner 
	    (mutable child)
	    (mutable child-count (serialization gzochi:integer-serialization)))
    (parent managed-sequence-node)
    (protocol (lambda (n) 
		(lambda (owner)
		  (let ((p (n #f #f #f 0)))
		    (let* ((tn (p owner #f 1))
			   (ln (make-managed-sequence-list-node tn 10)))
		      (managed-sequence-tree-node-child-set! tn ln)
		      tn))))))
 
  (define (managed-sequence-tree-node-split! node)
    (let ((child (managed-sequence-tree-node-child node)))
      (or (managed-sequence-node-parent node)
	  (let ((parent (make-managed-sequence-tree-node
			 (managed-sequence-tree-node-owner node))))
	    (managed-sequence-node-parent-set! node parent)
	    (managed-sequence-root-set! 
	     (managed-sequence-tree-node-owner node) parent)))
	    
      (let* ((new-node (make-managed-sequence-tree-node
			(managed-sequence-tree-node-owner node)))
	     (next (managed-sequence-node-next node))
	     (child-count (managed-sequence-tree-node-child-count node))
	     (half (div child-count 2)))
	
	(let loop ((child child)
		   (new-child child)
		   (child-count child-count)
		   (size 0)
		   (i 0))

	  (if (and child (< i child-count))
	      (if (eqv? i half)
		  (begin
		    (if (managed-sequence-tree-node? child)
			(begin
			  (managed-sequence-node-next-set!
			   (managed-sequence-node-prev child) #f)
			  (managed-sequence-node-prev-set! child #f)))
		    (loop child child child-count size (+ i 1)))
		  (let ((ii (+ i 1)))
		    (if (>= ii half)
			(begin
			  (managed-sequence-node-parent-set! child new-child)
			  (loop (managed-sequence-node-next child) new-child
				(+ child-count 1)
				(+ size (managed-sequence-node-size child))
				ii))
			(loop (managed-sequence-node-next child) new-child
			      child-count size ii))))
	      (begin
		(managed-sequence-tree-node-child-set! new-node)
		(managed-sequence-tree-node-child-count-set! 
		 node (- (managed-sequence-tree-node-child-count node) 
			 child-count))
		(managed-sequence-node-size-set!
		 node (- (managed-sequence-node-size node) size)))))

	(managed-sequence-node-prev-set! new-node node)
	(managed-sequence-node-next-set! node new-node)
	(if next 
	    (begin
	      (managed-sequence-node-next-set! new-node next)
	      (managed-sequence-node-prev-set! next new-node))))))

  (define (managed-sequence-tree-node-prune! node)
    (let ((prev (managed-sequence-node-prev node))
	  (next (managed-sequence-node-next node)))
      (if prev
	  (if next
	      (begin
		(managed-sequence-node-next-set! prev next)
		(managed-sequence-node-prev-set! next prev))
	      (managed-sequence-node-next-set! prev #f))
	  (let ((parent (managed-sequence-node-parent node)))
	    (if next
		(begin
		  (managed-sequence-node-prev-set! next #f)
		  (managed-sequence-tree-node-child-set! 
		   next (managed-sequence-node-size parent)
		   (managed-sequence-tree-node-child-count parent)))
		(managed-sequence-tree-node-child-set! parent #f)))))
		
    (gzochi:remove-object! node))

  (define* (managed-sequence-tree-node-increment! node #:optional split?)
    (define (count-children child)
      (define (count-children-inner child count)
	(if child 
	    (count-children-inner 
	     (managed-sequence-node-next child) (+ count 1)) 
	    count))
      (count-children-inner child 0))

    (let ((parent (managed-sequence-node-parent node)))
      (if parent
	  (begin
	    (managed-sequence-node-size-set! 
	     node (+ (managed-sequence-node-size node) 1))
	    (if split?
		(let ((child-count
		       (managed-sequence-tree-node-child-count node)))
		  (managed-sequence-tree-node-child-count-set!
		   node (+ child-count 1))
		  (if (>= child-count 5)
		      (begin 
			(managed-sequence-tree-node-split! node)
			(managed-sequence-tree-node-increment! parent #f))
		      (managed-sequence-tree-node-increment! parent split?)))))
		
	  (if split?
	      (let ((root-children (count-children node)))
		(if (> root-children 5)
		    (begin
		      (managed-sequence-node-size-set! 
		       (gzochi:managed-sequence-size 
			(managed-sequence-tree-node-owner node))
		      (managed-sequence-tree-node-child-count-set! 
		       node root-children)
		      (managed-sequence-tree-node-split! node)))))))))

  (define* (managed-sequence-tree-node-decrement! node #:optional prune?)
    (let ((parent (managed-sequence-node-parent node))
	  (size (managed-sequence-node-size node)))
      (and parent
	   (begin 
	     (managed-sequence-node-size-set! node (- size 1))
	     (if prune?
		 (let ((child-count 
			(managed-sequence-tree-node-child-count node)))	
		   (managed-sequence-node-size-set! node (- child-count 1))
		   (if (and (eqv? child-count 1) (eqv? size 1))
		       (begin
			 (managed-sequence-tree-node-prune! node)
			 (managed-sequence-tree-node-decrement! parent prune?))
		       (managed-sequence-tree-node-decrement! parent #f))))))))

  (gzochi:define-managed-record-type managed-sequence-connector 
   (fields (mutable target)))

  (gzochi:define-managed-record-type
   (managed-sequence gzochi:make-managed-sequence gzochi:managed-sequence?)

   (fields (mutable root) (mutable head) (mutable tail))

   (protocol (lambda (p) 
	       (lambda ()
		 (let* ((seq (p #f #f #f))
			(root (make-managed-sequence-tree-node seq))
			(child (managed-sequence-tree-node-child root)))
		   (managed-sequence-root-set! seq root)
		   (managed-sequence-head-set! 
		    seq (make-managed-sequence-connector child))
		   (managed-sequence-tail-set! 
		    seq (make-managed-sequence-connector child))
		   seq))))
   (sealed #t))

  (define (search-managed-sequence node from to)
    (cond
     ((managed-sequence-tree-node? node)
      (let ((offset (+ from (managed-sequence-node-size node))))
	(if (< offset to)
	    (search-managed-sequence 
	     (or (managed-sequence-node-next node)
		 (assertion-violation
		  'search-managed-sequence "Index out of bounds."))
	     offset to)
	    (search-managed-sequence
	     (managed-sequence-tree-node-child node) from to))))
     ((managed-sequence-list-node? node)
      (let ((offset (+ from (managed-sequence-node-size node))))
	(if (< offset to)
	    (search-managed-sequence
	     (or (managed-sequence-node-next node)
		 (assertion-violation
		  'search-managed-sequence "Index out of bounds."))
	     offset to)
	    (values node (- (- to from) 1)))))

     (else (assertion-violation
	    'search-managed-sequence "Expected list or tree node." node))))

  ;; Inserts the specified value at the specified index in the specified managed
  ;; sequence, shifting the sequence's contents to make room. The specified
  ;; serialization is used to wrap unmanaged values as managed serializables if
  ;; necessary.
  
  (define* (gzochi:managed-sequence-insert! seq i obj #:key serialization)
    (or (>= i 0) (assertion-violation
		  'gzochi:managed-sequence-insert! "Index out of bounds." i))
    (let ((head (managed-sequence-connector-target 
		 (managed-sequence-head seq)))
	  (tail (managed-sequence-connector-target
		 (managed-sequence-tail seq))))
	  
    (cond
     ((and (eq? head tail) (zero? (managed-sequence-node-size head)))
      (managed-sequence-list-node-append! tail obj serialization))
     ((not head)
      (assertion-violation 'gzochi:managed-sequence-insert! "Missing head."))
     ((zero? i) (managed-sequence-list-node-prepend! head obj serialization))
     (else (receive (node index)
	     (search-managed-sequence 
	      (managed-sequence-tree-node-child (managed-sequence-root seq))
	      0 (+ i 1))
	     (managed-sequence-list-node-insert!
	      node index obj serialization))))
    (and-let* ((new-tail (managed-sequence-node-next tail)))
      (managed-sequence-connector-target-set! 
       (managed-sequence-tail seq) new-tail))))

  (define (gzochi:managed-sequence-ref seq i)
    (or (>= i 0) (assertion-violation
		  'gzochi:managed-sequence-ref "Index out of bounds." i))
    
    (receive (node index)
      (search-managed-sequence 
       (managed-sequence-tree-node-child (managed-sequence-root seq)) 
       0 (+ i 1))
      (and-let* ((subseq (managed-sequence-list-node-subsequence node)))
	(gzochi:managed-vector-ref 
	 (managed-sequence-subsequence-contents subseq) index))))

  ;; Overwrites the value at the specified index in the specified managed
  ;; sequence with the specified value. The specified serialization is used to
  ;; wrap unmanaged values as managed serializables if necessary.

  (define* (gzochi:managed-sequence-set! seq i obj #:key serialization)
    (or (>= i 0) (assertion-violation
		  'gzochi:managed-sequence-set! "Index out of bounds." i))
		  
    (receive (node index)
      (search-managed-sequence 
       (managed-sequence-tree-node-child (managed-sequence-root seq)) 
       0 (+ i 1))
      (and-let* ((subseq (managed-sequence-list-node-subsequence node)))
	(gzochi:managed-vector-set! 
	 (managed-sequence-subsequence-contents subseq) index obj 
	 #:serialization serialization))))

  (define (gzochi:managed-sequence-size seq)
    (define (managed-sequence-size-inner node size)
      (if node
	  (managed-sequence-size-inner 
	   (managed-sequence-node-next node) 
	   (+ (managed-sequence-node-size node) size))
	  size))
    (managed-sequence-size-inner 
     (managed-sequence-tree-node-child (managed-sequence-root seq)) 0))

  (define* (gzochi:managed-sequence-delete! seq obj #:optional pred)
    (let loop ((node (managed-sequence-connector-target 
		      (managed-sequence-head seq))))
      (and node
	   (or (managed-sequence-list-node-delete! node obj pred)
	       (loop (managed-sequence-node-next node))))))

  (define (gzochi:managed-sequence-delete-at! seq i)
    (or (>= i 0) (assertion-violation
		  'gzochi:managed-sequence-delete-at! "Index out of bounds." i))
    
    (receive (node index)
      (search-managed-sequence 
       (managed-sequence-tree-node-child (managed-sequence-root seq)) 
       0 (+ i 1))
      (managed-sequence-list-node-delete-at! node index)))

  ;; Appends the specified value to the end of the specified managed sequence.
  ;; The specified serialization is used to wrap the value as a managed
  ;; serializable if necessary.
  
  (define* (gzochi:managed-sequence-add! seq obj #:key serialization)
    (let ((tail (managed-sequence-connector-target 
		 (managed-sequence-tail seq))))
      (managed-sequence-list-node-append! tail obj serialization)
      (and-let* ((new-tail (managed-sequence-node-next tail)))
	(managed-sequence-connector-target-set! 
	 (managed-sequence-tail seq) new-tail))))

  (define (gzochi:managed-sequence-fold-left seq fold-fn . seeds)
    (define seeds-length (length seeds))
    (define (terminate? seeds) (and (eqv? (length seeds) 1) (not (car seeds))))
    (define (managed-vector-fold-left mvec size . seeds)
      (let loop ((i 0) (seeds seeds))
	(if (< i size)
	    (receive seeds 
              (apply fold-fn (cons (gzochi:managed-vector-ref mvec i) seeds))
	      (let ((num-seeds (length seeds)))
		(cond ((terminate? seeds) #f)
		      ((eqv? num-seeds seeds-length) (loop (+ i 1) seeds))
		      (else (assertion-violation
			     'gzochi:managed-sequence-fold-left
			     "Seed length mismatch." num-seeds seeds-length)))))
	    (apply values seeds))))
	
    (define (managed-sequence-list-node-fold-left node . seeds)
      (let loop ((node node) (seeds seeds))
	(if node
	    (let* ((subseq (managed-sequence-list-node-subsequence node))
		   (mvec (managed-sequence-subsequence-contents subseq))
		   (size (managed-sequence-subsequence-size subseq)))
	      (receive seeds
	        (apply managed-vector-fold-left (cons* mvec size seeds))
		(and (not (terminate? seeds))
		     (loop (managed-sequence-node-next node) seeds))))
	    (apply values seeds))))

    (apply managed-sequence-list-node-fold-left
	   (cons (managed-sequence-connector-target
		  (managed-sequence-head seq)) 
		 seeds)))
  
  (define (gzochi:managed-sequence-fold-right seq fold-fn . seeds)
    (define seeds-length (length seeds))
    (define (terminate? seeds) (and (eqv? (length seeds) 1) (not (car seeds))))
    (define (managed-vector-fold-right mvec size . seeds)
      (let loop ((i (- size 1)) (seeds seeds))
	(if (>= i 0)
	    (receive seeds 
              (apply fold-fn (cons (gzochi:managed-vector-ref mvec i) seeds))
	      (let ((num-seeds (length seeds)))
		(cond ((terminate? seeds) #f)
		      ((eqv? num-seeds seeds-length) (loop (- i 1) seeds))
		      (else (assertion-violation
			     'gzochi:managed-sequence-fold-right
			     "Seed length mismatch." num-seeds seeds-length)))))
	    (apply values seeds))))
	
    (define (managed-sequence-list-node-fold-right node . seeds)
      (let loop ((node node) (seeds seeds))
	(if node
	    (let* ((subseq (managed-sequence-list-node-subsequence node))
		   (mvec (managed-sequence-subsequence-contents subseq))
		   (size (managed-sequence-subsequence-size subseq)))
	      (receive seeds
	        (apply managed-vector-fold-right (cons* mvec size seeds))
		(and (not (terminate? seeds))
		     (loop (managed-sequence-node-prev node) seeds))))
	    (apply values seeds))))

    (apply managed-sequence-list-node-fold-right
	   (cons (managed-sequence-connector-target 
		  (managed-sequence-tail seq))
		 seeds)))

  (define* (gzochi:managed-sequence-contains? seq obj #:optional (pred eq?))
    (let ((ret #f))
      (gzochi:managed-sequence-fold-left 
       seq (lambda (obj2) (if (pred obj obj2) (begin (set! ret #t) #f))))
      ret))

  (define (gzochi:managed-sequence->list seq)
    (gzochi:managed-sequence-fold-right seq cons '()))

  (define-record-type managed-hashtable-entry
    (fields (immutable hash) ;; The hash code of the key

	    ;; Whether the key is stored "inline" and wrapped in a
	    ;; serialization reference that needs to be removed on retrieval
	    
	    (immutable wrapped-key?)

	    ;; A reference to the key
	    
	    (immutable key managed-hashtable-entry-key-internal) 

	    ;; Whether the value is stored "inline" and wrapped in a
	    ;; serialization reference that needs to be removed on retrieval

	    (mutable wrapped-value?
		     managed-hashtable-entry-wrapped-value?
		     managed-hashtable-entry-wrapped-value-set!)

	    ;; A reference to the value
	    
	    (mutable value
		     managed-hashtable-entry-value-internal
		     managed-hashtable-entry-value-internal-set!)

	    (mutable next)) ;; Linked list of entries with the same hash

   (protocol 
    (lambda (p)
      (lambda (hash key key-serialization value value-serialization)
	(or (gzochi:managed-record? key)
	    (gzochi:serialization-reference? key-serialization)
	    (assertion-violation
	     'make-managed-hashtable-entry
	     "Serialization must be specified for unmanaged keys."))
	(or (gzochi:managed-record? value)
	    (gzochi:serialization-reference? value-serialization)
	    (assertion-violation
	     'make-managed-hashtable-entry
	     "Serialization must be specified for unmanaged values."))

	(let* ((wrapped-key (not (gzochi:managed-record? key)))
	       (key (if wrapped-key
			(cons key-serialization key)
			(gzochi:create-reference key)))
	       (wrapped-value (not (gzochi:managed-record? value)))
	       (value (if wrapped-value
			  (cons value-serialization value)
			  (gzochi:create-reference value))))
	  
	  (p hash wrapped-key key wrapped-value value #f))))))

  ;; Serializer for `managed-hashtable-entry'. Tail-recursively invokes itself
  ;; to serialize subsequent entries in the hash chain.
  
  (define (serialize-managed-hashtable-entry port entry)
    (gzochi:write-integer port (managed-hashtable-entry-hash entry))
    (let ((wrapped-key? (managed-hashtable-entry-wrapped-key? entry))
	  (key (managed-hashtable-entry-key-internal entry)))
      (gzochi:write-boolean port wrapped-key?)
      (if wrapped-key?
	  (serialize-serialization-with-value port key)
	  (gzochi:write-integer port (gzochi:managed-reference-oid key))))
    (let ((wrapped-value? (managed-hashtable-entry-wrapped-value? entry))
	  (value (managed-hashtable-entry-value-internal entry)))
      (gzochi:write-boolean port wrapped-value?)
      (if wrapped-value?
	  (serialize-serialization-with-value port value)
	  (gzochi:write-integer port (gzochi:managed-reference-oid value))))

    (let ((next (managed-hashtable-entry-next entry)))

      ;; Are there more entries in the chain?
      
      (if next
	  (begin
	    (gzochi:write-boolean port #t)
	    (serialize-managed-hashtable-entry port next))
	  (gzochi:write-boolean port #f))))

  ;; Deserializer for `managed-hashtable-entry'. Tail-recursively deserializes
  ;; additional entries in the hash chain.
  
  (define (deserialize-managed-hashtable-entry port)

    ;; Need a version of the `managed-hashtable-entry' constructor that lets us
    ;; specify the key and value directly, without attempting wrap unmanaged
    ;; objects.
    
    (define raw-constructor
      (record-constructor
       (make-record-constructor-descriptor managed-hashtable-entry #f #f)))

    ;; Given a list of unlinked `managed-hashtable-entry' records, link them
    ;; together via their `next' fields.
    
    (define (connect chain)
      (let loop ((head (car chain)) (tail (cdr chain)))
	(if (null? tail)
	    head
	    (let ((tail-head (car tail)))
	      (managed-hashtable-entry-next-set! tail-head head)
	      (loop tail-head (cdr tail))))))
    
    (define (read-chained chain)
      (let* ((hash (gzochi:read-integer port))
	     (wrapped-key? (gzochi:read-boolean port))
	     (key (if wrapped-key?
		      (deserialize-serialization-with-value port)
		      (gzochi:make-managed-reference
		       (gzochi:read-integer port))))
	     (wrapped-value? (gzochi:read-boolean port))
	     (value (if wrapped-value?
			(deserialize-serialization-with-value port)
			(gzochi:make-managed-reference
			 (gzochi:read-integer port))))
	     
	     (partial (raw-constructor
		       hash wrapped-key? key wrapped-value? value #f)))
	
	(if (gzochi:read-boolean port)
	    (read-chained (cons partial chain))
	    (connect (cons partial chain)))))

    ;; Initiate the deserialization.
    
    (read-chained '()))

  ;; Convenience function to read the key from a `managed-hashtable-entry',
  ;; unwrapping managed serializable records as necessary.
  
  (define (managed-hashtable-entry-key entry)
    (let ((key (managed-hashtable-entry-key-internal entry)))
      (if (managed-hashtable-entry-wrapped-key? entry)
	  (cdr key)
	  (gzochi:dereference key))))
  
  ;; Convenience function to read the value from a `managed-hashtable-entry',
  ;; unwrapping managed serializable records as necessary.

  (define (managed-hashtable-entry-value entry)
    (let ((value (managed-hashtable-entry-value-internal entry)))
      (if (managed-hashtable-entry-wrapped-value? entry)
	  (cdr value)
	  (gzochi:dereference value))))

  (define (managed-hashtable-entry-value-set! entry value value-serialization)
    (if (gzochi:managed-record? value)
	(begin
	  (managed-hashtable-entry-value-internal-set!
	   entry (gzochi:create-reference value))
	  (managed-hashtable-entry-wrapped-value-set! entry #f))
	(begin
	  (or (gzochi:serialization-reference? value-serialization)
	      (assertion-violation
	       'managed-hashtable-entry-value-set!
	       "Serialization must be specified for unmanaged values."))
	  (managed-hashtable-entry-value-internal-set!
	   entry (cons value-serialization value))
	  (managed-hashtable-entry-wrapped-value-set! entry #t))))

  (gzochi:define-managed-record-type managed-hashtable-node
   (fields (mutable parent)
	   (mutable left-leaf)
	   (mutable right-leaf) 
	   (mutable directory)
	   (mutable entries (serialization
			     (gzochi:make-uniform-vector-serialization
			      (gzochi:make-serialization
			       serialize-managed-hashtable-entry
			       deserialize-managed-hashtable-entry))))

	   (immutable depth (serialization gzochi:integer-serialization))
	   (mutable size (serialization gzochi:integer-serialization)))

   (protocol (lambda (p)
	       (lambda (depth) (p #f #f #f #f (make-vector 256 #f) depth 0))))
   (sealed #t))

  (gzochi:define-managed-record-type 
    (gzochi:managed-hashtable 
     gzochi:make-managed-hashtable 
     gzochi:managed-hashtable?)

    (fields hash-function equivalence-function root)

    (protocol (lambda (p)
		(lambda (hash-callback equiv-callback)
		  (let ((root (make-managed-hashtable-node 0)))
		    (managed-hashtable-node-ensure-depth! root 5)
		    (p hash-callback equiv-callback root)))))
    (sealed #t))

  (define (high-bits n num-bits)
    (bitwise-arithmetic-shift-right n (- 32 num-bits)))
  (define max-dir-bits 5)
  (define (node-dir-bits depth) (min (- 32 depth) max-dir-bits))

  (define (managed-hashtable-node-add-leaves! node prefix left-leaf right-leaf)
    (let* ((depth (managed-hashtable-node-depth node))
	   (prefix (bitwise-and 
		    (bitwise-arithmetic-shift-left prefix depth)
		    (- (bitwise-arithmetic-shift-left 1 32) 1)))
	   (dir-bits (node-dir-bits depth))
	   (directory (managed-hashtable-node-directory node))  
	   (index (high-bits prefix dir-bits))
	   (leaf (gzochi:managed-vector-ref directory index)))

      (gzochi:remove-object! leaf)
      (managed-hashtable-node-parent-set! left-leaf node)
      (managed-hashtable-node-parent-set! right-leaf node)
      
      (let* ((sig-bits (- (managed-hashtable-node-depth leaf) depth))
	     (mask (bitwise-arithmetic-shift-left
		    (- (bitwise-arithmetic-shift-left 1 sig-bits) 1)
		    (- dir-bits sig-bits)))

	     (left (bitwise-and index mask))
	     (num-each
	      (bitwise-arithmetic-shift-left 1 (- (- dir-bits sig-bits) 1)))
	     (right (+ left num-each))
	     (left-total (+ left num-each))
	     (right-total (+ right num-each)))

	(let loop ((i left))
	  (or (eqv? i left-total)
	      (begin
		(gzochi:managed-vector-set! directory i left-leaf)
		(loop (+ i 1)))))

	(let loop ((i right))
	  (or (eqv? i right-total)
	      (begin
		(gzochi:managed-vector-set! directory i right-leaf)
		(loop (+ i 1))))))))

  (define (managed-hashtable-node-split! node)
    (or (> (vector-length (managed-hashtable-node-entries node)) 0)
	(assertion-violation
	 'managed-hashtable-node-split! "Can't split a directory node!"))
    (let* ((depth (managed-hashtable-node-depth node))
	   (entries (managed-hashtable-node-entries node))
	   (num-entries (vector-length entries))
	   (left-child (make-managed-hashtable-node (+ depth 1)))
	   (right-child (make-managed-hashtable-node (+ depth 1)))
	   (first-right (/ num-entries 2))
	   (prefix
	    (let loop ((i 0) (prefix 0))
	      (let* ((child (if (< i first-right) left-child right-child))
		     (entry (and (< i num-entries) (vector-ref entries i)))
		     (next-prefix
		      (and entry
			   (let loop ((entry entry) (prev #f) (prev-index 0))
			     (let* ((h (managed-hashtable-entry-hash entry))
				    (x (managed-hashtable-leaf-index child h))
				    (ne (managed-hashtable-entry-next entry)))
			       (managed-hashtable-node-add-entry!
				child entry (and (eqv? x prev-index) prev))
			       (if ne (loop ne entry x) h))))))
		(if (< i num-entries) 
		    (loop (+ i 1) (or next-prefix prefix)) 
		    prefix)))))

      (managed-hashtable-node-entries-set! node (make-vector 0))
      (managed-hashtable-node-size-set! node 0)

      (let ((left-leaf (managed-hashtable-node-left-leaf node)))
	(if left-leaf
	    (begin
	      (managed-hashtable-node-right-leaf-set! left-leaf left-child)
	      (managed-hashtable-node-left-leaf-set! left-child left-leaf)
	      (managed-hashtable-node-left-leaf-set! node #f))))
      (let ((right-leaf (managed-hashtable-node-right-leaf node)))
	(if right-leaf
	    (begin
	      (managed-hashtable-node-left-leaf-set! right-leaf right-child)
	      (managed-hashtable-node-right-leaf-set! right-child right-leaf)
	      (managed-hashtable-node-right-leaf-set! node #f))))

      (managed-hashtable-node-right-leaf-set! left-child right-child)
      (managed-hashtable-node-left-leaf-set! right-child left-child)

      (if (or (not (managed-hashtable-node-parent node))
	      (eqv? (modulo depth max-dir-bits) 0)
	      (eqv? depth 6))
	  (begin
	    (managed-hashtable-node-parent-set! right-child node)
	    (managed-hashtable-node-parent-set! left-child node)
	    (let* ((directory-length 
		    (bitwise-arithmetic-shift-left 1 (node-dir-bits depth)))
		   (directory (gzochi:make-managed-vector directory-length)))
	      (managed-hashtable-node-directory-set! node directory)
	      (let ((first-right (/ directory-length 2)))
		(let loop ((i 0))
		  (or (eqv? i directory-length)
		      (begin
			(gzochi:managed-vector-set! 
			 directory i (if (< i first-right) 
					 left-child 
					 right-child))
			(loop (+ i 1))))))))
	  
	  (managed-hashtable-node-add-leaves! 
	   (managed-hashtable-node-parent node) prefix left-child 
	   right-child))))

  (define (managed-hashtable-node-ensure-depth! node min-depth)
    (let ((depth (managed-hashtable-node-depth node))) 
      (or (>= depth min-depth)
	  (let* ((directory-length
		  (bitwise-arithmetic-shift-left 1 (node-dir-bits depth)))
		 (directory (gzochi:make-managed-vector directory-length)))
	    (managed-hashtable-node-entries-set! node (make-vector 0))
	    (managed-hashtable-node-directory-set! node directory)
	    (let* ((leaf-bits (min (- min-depth depth) max-dir-bits))
		   (num-leaves (bitwise-arithmetic-shift-left 1 leaf-bits))
		   (leaves (gzochi:make-managed-vector num-leaves)))
	      (let loop ((i 0))
		(or (eqv? i num-leaves)
		    (let ((leaf (make-managed-hashtable-node 
				 (+ depth leaf-bits))))
		      (gzochi:managed-vector-set! leaves i leaf)
		      (managed-hashtable-node-parent-set! leaf node)
		      (loop (+ i 1)))))
	      (let loop ((i 1))
		(or (eqv? i (- num-leaves 1))
		    (let ((leaf (gzochi:managed-vector-ref leaves i)))
		      (managed-hashtable-node-left-leaf-set! 
		       leaf (gzochi:managed-vector-ref leaves (- i 1)))
		      (managed-hashtable-node-right-leaf-set!
		       leaf (gzochi:managed-vector-ref leaves (+ i 1)))
		      (loop (+ i 1)))))
	      (let ((left-leaf (managed-hashtable-node-left-leaf node))
		    (right-leaf (managed-hashtable-node-right-leaf node))
		    (first-leaf (gzochi:managed-vector-ref leaves 0))
		    (last-leaf (gzochi:managed-vector-ref 
				leaves (- num-leaves 1))))
		(managed-hashtable-node-left-leaf-set! first-leaf left-leaf)
		(if left-leaf 
		    (managed-hashtable-node-right-leaf-set! 
		     left-leaf first-leaf))
		(managed-hashtable-node-right-leaf-set! 
		 first-leaf (gzochi:managed-vector-ref leaves 1))
		(managed-hashtable-node-left-leaf-set! 
		 last-leaf (gzochi:managed-vector-ref leaves (- num-leaves 2)))
		(managed-hashtable-node-right-leaf-set! last-leaf right-leaf)
		(if right-leaf
		    (managed-hashtable-node-left-leaf-set! 
		     right-leaf last-leaf))
		
		(managed-hashtable-node-left-leaf-set! node #f)
		(managed-hashtable-node-right-leaf-set! node #f))
	      
	      ;; Fill the directory.
	      
	      (let* ((entries-per-leaf (/ directory-length num-leaves)))
		(let loop ((i 0) (pos 0))
		  (or (eqv? i num-leaves)
		      (let ((leaf (gzochi:managed-vector-ref leaves i))
			    (next-pos (+ pos entries-per-leaf)))
			(let loop ((j pos))
			  (or (eqv? j next-pos)
			      (begin
				(gzochi:managed-vector-set! directory j leaf)
				(loop (+ j 1)))))
			(loop (+ i 1) next-pos)))))
	      
	      ;; Ensure depth for new leaves.
	      
	      (let loop ((i 0))
		(or (eqv? i num-leaves)
		    (begin
		      (managed-hashtable-node-ensure-depth! 
		       (gzochi:managed-vector-ref leaves i) min-depth)
		      (loop (+ i 1)))))
	      (if #f #f))))))

  (define (managed-hashtable-node-add-entry! node entry prev)
    (managed-hashtable-node-size-set!
     node (+ (managed-hashtable-node-size node) 1))
    (if prev
	(let ((next (managed-hashtable-entry-next prev)))
	  (managed-hashtable-entry-next-set! prev entry)
	  (managed-hashtable-entry-next-set! entry next))
	(let ((index (managed-hashtable-leaf-index
		      node (managed-hashtable-entry-hash entry)))
	      (table (managed-hashtable-node-entries node)))
	  (managed-hashtable-entry-next-set! entry (vector-ref table index))
	  (vector-set! table index entry))))
  
  (define (managed-hashtable-node-add-entry-and-split! node entry prev)
    (managed-hashtable-node-add-entry! node entry prev)
    (if (and (> (managed-hashtable-node-size node) 98)
	     (< (managed-hashtable-node-depth node) 31))
	(managed-hashtable-node-split! node)))

  (define (managed-hashtable-node-lookup node prefix)
    (let loop ((node node))
      (if (eqv? (vector-length (managed-hashtable-node-entries node)) 0)
	  (let* ((depth (managed-hashtable-node-depth node))
		 (directory (managed-hashtable-node-directory node))
		 (index (high-bits
			 (bitwise-and
			  (bitwise-arithmetic-shift-left prefix depth)
			  (- (bitwise-arithmetic-shift-left 1 32) 1))
			 (node-dir-bits depth))))
	    (loop (gzochi:managed-vector-ref directory index)))
	  node)))

  (define* (gzochi:managed-hashtable-set! ht key value #:key
					  key-serialization
					  value-serialization)
					  
    (let* ((hash-function (gzochi:managed-hashtable-hash-function ht))
	   (hash-function (gzochi:resolve-procedure
			   (gzochi:callback-procedure hash-function)
			   (gzochi:callback-module hash-function)))

	   (equiv-function (gzochi:managed-hashtable-equivalence-function ht))
	   (equiv-function (gzochi:resolve-procedure
			   (gzochi:callback-procedure equiv-function)
			   (gzochi:callback-module equiv-function)))

	   (hash (if key (hash-function key) 0))
	   (leaf (managed-hashtable-node-lookup 
		  (gzochi:managed-hashtable-root ht) hash)))
    
      (let loop ((entry (vector-ref (managed-hashtable-node-entries leaf)
				    (managed-hashtable-leaf-index leaf hash)))
		 (prev #f))
	(if entry
	    (let ((entry-hash (managed-hashtable-entry-hash entry)))
	      (cond ((< entry-hash hash)
		     (loop (managed-hashtable-entry-next entry) entry))
		    ((not (eqv? entry-hash hash)) (loop #f prev))
 		    (else
		     (if (equiv-function
			  (managed-hashtable-entry-key entry) key)
			 (managed-hashtable-entry-value-set!
			  entry value value-serialization)
			 (loop (managed-hashtable-entry-next entry) 
			       entry)))))
	    (managed-hashtable-node-add-entry-and-split!
	     leaf (make-managed-hashtable-entry hash key key-serialization
						value value-serialization)
	     prev)))))

  (define (managed-hashtable-leaf-index node hash)
    (let* ((left-offset (min 32 (+ (managed-hashtable-node-depth node) 8))))
      (bitwise-and (high-bits hash left-offset) 255)))

  (define (managed-hashtable-get-entry ht key)
    (let* ((hash-function (gzochi:managed-hashtable-hash-function ht))
	   (hash-function (gzochi:resolve-procedure
			   (gzochi:callback-procedure hash-function)
			   (gzochi:callback-module hash-function)))

	   (equiv-function (gzochi:managed-hashtable-equivalence-function ht))
	   (equiv-function (gzochi:resolve-procedure
			   (gzochi:callback-procedure equiv-function)
			   (gzochi:callback-module equiv-function)))
	   
	   (hash (if key (hash-function key) 0))
	   (leaf (managed-hashtable-node-lookup 
		  (gzochi:managed-hashtable-root ht) hash)))

      (let loop ((entry (vector-ref (managed-hashtable-node-entries leaf)
				    (managed-hashtable-leaf-index leaf hash))))
	(and entry
	     (let ((entry-hash (managed-hashtable-entry-hash entry)))
	       (cond ((and (eqv? entry-hash hash)
			   (equiv-function
			    (managed-hashtable-entry-key entry) key))
		      entry)
		     ((< hash entry-hash) #f)
		     (else (loop (managed-hashtable-entry-next entry)))))))))

  (define (gzochi:managed-hashtable-contains? ht key)
    (if (managed-hashtable-get-entry ht key) #t #f))

  (define* (gzochi:managed-hashtable-update! 
	    ht key proc default #:key key-serialization value-serialization)

    (let ((entry (managed-hashtable-get-entry ht key)))
      (if entry
	  (managed-hashtable-entry-value-set! 
	   entry (proc (managed-hashtable-entry-value entry))
	   value-serialization)
	  (gzochi:managed-hashtable-set! 
	   ht key (proc default) 
	   #:key-serialization key-serialization
	   #:value-serialization value-serialization))))

  (define (gzochi:managed-hashtable-clear! ht)
    (define (clear-internal node)
      (let ((entries (managed-hashtable-node-entries node)))
	(if (eqv? (vector-length entries) 0)
	    (let* ((directory (managed-hashtable-node-directory node))
		   (len (gzochi:managed-vector-length directory)))
	      (let loop ((i 0))
		(if (< i len) 
		    (begin
		      (clear-internal 
		       (gzochi:managed-vector-ref directory i))
		      (loop (+ i 1))))))))

      (managed-hashtable-node-size-set! node 0)
      (managed-hashtable-node-left-leaf-set! node #f)
      (managed-hashtable-node-right-leaf-set! node #f)
      (gzochi:remove-object! node))
  
    (let ((root (gzochi:managed-hashtable-root ht)))
      (clear-internal root)
      (managed-hashtable-node-ensure-depth! root 5)))

  (define (gzochi:managed-hashtable-ref ht key default)
    (let ((entry (managed-hashtable-get-entry ht key)))
      (if entry
	  (managed-hashtable-entry-value entry)
	  default)))

  (define (gzochi:managed-hashtable-delete! ht key)
    (let* ((hash-function (gzochi:managed-hashtable-hash-function ht))
	   (hash-function (gzochi:resolve-procedure
			   (gzochi:callback-procedure hash-function)
			   (gzochi:callback-module hash-function)))

	   (equiv-function (gzochi:managed-hashtable-equivalence-function ht))
	   (equiv-function (gzochi:resolve-procedure
			   (gzochi:callback-procedure equiv-function)
			   (gzochi:callback-module equiv-function)))
	   
	   (hash (if key (hash-function key) 0))
	   (leaf (managed-hashtable-node-lookup 
		  (gzochi:managed-hashtable-root ht) hash))
	   (entries (managed-hashtable-node-entries leaf))
	   (index (managed-hashtable-leaf-index leaf hash)))

      (let loop ((entry (vector-ref entries index)) (prev #f))
	(and entry
	     (let ((entry-hash (managed-hashtable-entry-hash entry)))
	       (cond ((or (not entry) (> entry-hash hash)) (if #f #f))
		     ((and (eqv? entry-hash hash)
			   (equiv-function 
			    (managed-hashtable-entry-key entry) key))
		      (managed-hashtable-node-size-set! 
		       leaf (- (managed-hashtable-node-size leaf) 1))
		      (let ((next (managed-hashtable-entry-next entry)))
			(if prev
			    (managed-hashtable-entry-next-set! prev next)
			    (vector-set! entries index next))))
		     (else (loop (managed-hashtable-entry-next entry) 
				 entry))))))))

  (define (gzochi:managed-hashtable-size ht)
    (let ((cur (managed-hashtable-node-lookup 
		(gzochi:managed-hashtable-root ht) 0)))
      (let loop ((cur cur) (total-size (managed-hashtable-node-size cur)))
	(let ((next (managed-hashtable-node-right-leaf cur)))
	  (if next
	      (loop next (+ total-size (managed-hashtable-node-size next)))
	      total-size)))))

  (define (managed-hashtable-entries ht)
    (define (first-entry node)
      (let* ((table (managed-hashtable-node-entries node))
	     (table-length (vector-length table)))
	(let loop ((i 0))
	  (and (not (eqv? i table-length))
	       (or (vector-ref table i) (loop (+ i 1)))))))
    
    (let ((left-most 
	   (managed-hashtable-node-lookup 
	    (gzochi:managed-hashtable-root ht) 0)))
      
      (let loop ((current-leaf left-most)
		 (current-entry (first-entry left-most))
		 (entries (list)))
	(if current-entry
	    (loop current-leaf 
		  (managed-hashtable-entry-next current-entry) 
		  (cons current-entry entries))
	    (let ((next-leaf (managed-hashtable-node-right-leaf current-leaf)))
	      (if next-leaf
		  (loop next-leaf (first-entry next-leaf) entries)
		  (reverse entries)))))))

  ;; Returns two values: A managed vector containing the keys of the specified
  ;; managed hashtable, and a managed vector containing the values. The keys and
  ;; values in the returned managed vectors will be in corresponding relative
  ;; order.
  
  (define (gzochi:managed-hashtable-entries ht)
    (let* ((entries (managed-hashtable-entries ht))
	   (num-entries (length entries))
	   (key-vector (gzochi:make-managed-vector num-entries))
	   (value-vector (gzochi:make-managed-vector num-entries)))

      (let loop ((i 0) (entries entries))
	(or (eqv? i num-entries)
	    (let ((entry (car entries)))
	      (if (managed-hashtable-entry-wrapped-key? entry)
		  (let ((pair (managed-hashtable-entry-key-internal entry)))
		    (gzochi:managed-vector-set!
		     key-vector i (cdr pair) #:serialization (car pair)))
		  (gzochi:managed-vector-set! 
		   key-vector i (managed-hashtable-entry-key entry)))
	      (if (managed-hashtable-entry-wrapped-value? entry)
		  (let ((pair (managed-hashtable-entry-value-internal entry)))
		    (gzochi:managed-vector-set!
		     value-vector i (cdr pair) #:serialization (car pair)))
		  (gzochi:managed-vector-set! 
		   value-vector i (managed-hashtable-entry-value entry)))
	      (loop (+ i 1) (cdr entries)))))

      (values key-vector value-vector)))

  ;; Returns a managed vector containing the keys of the specified managed
  ;; hashtable.
  
  (define (gzochi:managed-hashtable-keys ht)
    (let* ((entries (managed-hashtable-entries ht))
	   (num-entries (length entries))
	   (key-vector (gzochi:make-managed-vector num-entries)))

      (let loop ((i 0) (entries entries))
	(if (< i num-entries)
	    (let ((entry (car entries)))
	      (if (managed-hashtable-entry-wrapped-key? entry)
		  (let ((pair (managed-hashtable-entry-key-internal entry)))
		    (gzochi:managed-vector-set!
		     key-vector i (cdr pair) #:serialization (car pair)))
		  (gzochi:managed-vector-set!
		   key-vector i (managed-hashtable-entry-key entry)))
	      
	      (loop (+ i 1) (cdr entries)))
	    key-vector))))
  
  (define (gzochi:serialize port obj)
    (or (gzochi:managed-record? obj)
	(assertion-violation
	 'gzochi:serialize "Only managed records may be auto-serialized." obj))

    (gzochi:serialize-managed-reference port (gzochi:create-reference obj)))

  (define (gzochi:deserialize port)
    (gzochi:dereference (gzochi:deserialize-managed-reference port)))
)
