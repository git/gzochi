/* taskmap.h: Generic interface for taskmap implementations
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GZOCHI_METAD_TASKMAP_H
#define GZOCHI_METAD_TASKMAP_H

#include <glib.h>

/*
  The following prototypes describe a mechanism for creating and consulting a 
  bi-directional mapping between gzochid application server nodes and the 
  durable tasks assigned to them, for use in load balancing and application
  resiliency.
*/

/* The base struct type for taskmap implementations. */

struct _gzochi_metad_taskmap
{
  struct _gzochi_metad_taskmap_iface *iface; /* The taskmap vtable. */
};

typedef struct _gzochi_metad_taskmap gzochi_metad_taskmap;

/* The taskmap functional interface, as a kind of poor man's vtable. */

struct _gzochi_metad_taskmap_iface
{
  /* Map the specified task id - which may refer to a new task or to a 
     currently "unmapped" task - to the specified application server node id.
     Signals an error if the task is already mapped to a server. */
  
  void (*map_task) (gzochi_metad_taskmap *, const char *, guint64, int,
		    GError **);

  /* Removes the mapping for the specified task id, moving the specified task to
     the set of unmapped tasks. Signals an error if the task is not currently
     mapped to a server. */
  
  void (*unmap_task) (gzochi_metad_taskmap *, const char *, guint64, GError **);

  /* Removes the specified task id entirely, whether or not it is currently
     mapped to an application server node. Signals an error if the task is not 
     currently mapped to a server or in the unmapped task set. */
  
  void (*remove_task) (gzochi_metad_taskmap *, const char *, guint64,
		       GError **);
  
  /* Returns the id of the node to which the specified task is assigned. Signals
     an error if the task is not mapped to any node. */
  
  int (*lookup_node) (gzochi_metad_taskmap *, const char *, guint64, GError **);
  
  /* Returns an array of all task ids (as guint64s) mapped to the specified 
     application server node id.

     The array should be unref'd when no longer in use. */
  
  GArray *(*lookup_mapped_tasks) (gzochi_metad_taskmap *, const char *, int);

  /* Returns an array of all task ids (as guint64s) not mapped to any 
     application server node. 

     The array should be unref'd when no longer in use. */

  GArray *(*lookup_unmapped_tasks) (gzochi_metad_taskmap *, const char *);
  
  /* Removes all tasks ids mapped to the specified application server node 
     id, moving them to the corresponding unmapped task sets. */
  
  void (*unmap_all) (gzochi_metad_taskmap *, int);
};

typedef struct _gzochi_metad_taskmap_iface gzochi_metad_taskmap_iface;

/* Convenience macro for extracting the functional interface from a given
   taskmap instance. */

#define GZOCHI_METAD_TASKMAP_IFACE(taskmap) \
  ((gzochi_metad_taskmap *) (taskmap))->iface;

enum
  {
    /* Indicates a failure resulting from an attempt to create a mapping for a
       task that is already mapped to another application server node id. */
    
    GZOCHI_METAD_TASKMAP_ERROR_ALREADY_MAPPED,

    /* Indicates a failure resulting from an attempt to unmap a task that is 
       not currently mapped to a server. */
    
    GZOCHI_METAD_TASKMAP_ERROR_NOT_MAPPED,

    /* Generic taskmap failure. */
    
    GZOCHI_METAD_TASKMAP_ERROR_FAILED
  };

#define GZOCHI_METAD_TASKMAP_ERROR gzochi_metad_taskmap_error_quark ()

GQuark gzochi_metad_taskmap_error_quark ();

#endif /* GZOCHI_METAD_TASKMAP_H */
