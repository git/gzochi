/* test-durable-task.c: Test routines for durable-task.c in gzochid.
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <libguile.h>
#include <stddef.h>
#include <stdlib.h>

#include "app.h"
#include "app-factory.h"
#include "app-store.h"
#include "app-task.h"
#include "config.h"
#include "data.h"
#include "durable-task.h"
#include "game.h"
#include "guile.h"
#include "gzochid-auth.h"
#include "oids.h"
#include "oids-storage.h"
#include "schedule.h"
#include "scheme.h"
#include "scheme-task.h"
#include "storage-mem.h"

static GMutex test_worker_mutex;
static GCond test_worker_cond;
static gboolean test_complete;

struct _test_context
{
  GzochidApplicationContext *app_context;
  gzochid_auth_identity *identity;
};

typedef struct _test_context test_context;

static GString *test_worker_string;

static void
serialize_test_worker_string (GzochidApplicationContext *context,
			      gpointer data, GByteArray *out, GError **error)
{
}

static gpointer
deserialize_test_worker_string (GzochidApplicationContext *context,
				GByteArray *in, GError **error)
{
  return test_worker_string;
}

static void
finalize_test_worker_string (GzochidApplicationContext *context, gpointer data)
{
}

static gzochid_io_serialization test_worker_string_serialization =
  {
    serialize_test_worker_string,
    deserialize_test_worker_string,
    finalize_test_worker_string
  };

static void
serialize_task_worker_noop (GzochidApplicationContext *context,
			    gzochid_application_worker worker, GByteArray *out)
{
}

static void
test_string_worker_a (GzochidApplicationContext *context,
		      gzochid_auth_identity *identity, gpointer data)
{
  GString *str = data;
  g_string_append (str, "a");
}

static gzochid_application_worker
deserialize_task_worker_a (GzochidApplicationContext *context, GByteArray *in)
{
  return test_string_worker_a;
}

static gzochid_application_worker_serialization test_worker_serialization_a =
  { serialize_task_worker_noop, deserialize_task_worker_a };

static gzochid_application_task_serialization string_task_serialization_a =
  {
    "string-task-serialization-a",
    &test_worker_serialization_a,
    &test_worker_string_serialization
  };

static void
test_string_worker_b (GzochidApplicationContext *context,
		      gzochid_auth_identity *identity, gpointer data)
{
  GString *str = data;
  g_string_append (str, "b");
}

static gzochid_application_worker
deserialize_task_worker_b (GzochidApplicationContext *context, GByteArray *in)
{
  return test_string_worker_b;
}

static gzochid_application_worker_serialization test_worker_serialization_b =
  { serialize_task_worker_noop, deserialize_task_worker_b };

static gzochid_application_task_serialization string_task_serialization_b =
  {
    "string-task-serialization-b",
    &test_worker_serialization_b,
    &test_worker_string_serialization
  };

static void
test_string_worker_c (GzochidApplicationContext *context,
		      gzochid_auth_identity *identity, gpointer data)
{
  GString *str = data;
  g_string_append (str, "c");

  g_mutex_lock (&test_worker_mutex);
  test_complete = TRUE;
  g_cond_signal (&test_worker_cond);
  g_mutex_unlock (&test_worker_mutex);
}

static gzochid_application_worker
deserialize_task_worker_c (GzochidApplicationContext *context, GByteArray *in)
{
  return test_string_worker_c;
}

static gzochid_application_worker_serialization test_worker_serialization_c =
  { serialize_task_worker_noop, deserialize_task_worker_c };

static gzochid_application_task_serialization string_task_serialization_c =
  {
    "string-task-serialization-c",
    &test_worker_serialization_c,
    &test_worker_string_serialization
  };

static void 
test_task_chain_inner0 (gpointer data)
{
  test_context *context = data;
  struct timeval immediate = { 0, 0 };

  test_worker_string = g_string_new ("");
  
  gzochid_application_task *task_a = gzochid_application_task_new
    (context->app_context, context->identity, test_string_worker_a, NULL,
     test_worker_string);
  gzochid_application_task *task_b = gzochid_application_task_new
    (context->app_context, context->identity, test_string_worker_b, NULL,
     test_worker_string);
  gzochid_application_task *task_c = gzochid_application_task_new
    (context->app_context, context->identity, test_string_worker_c, NULL,
     test_worker_string);

  gzochid_durable_application_task_handle *handle_a =
    gzochid_create_durable_application_task_handle
    (task_a, &string_task_serialization_a, immediate, NULL, NULL);
  gzochid_durable_application_task_handle *handle_b =
    gzochid_create_durable_application_task_handle
    (task_b, &string_task_serialization_b, immediate, NULL, NULL);
  gzochid_durable_application_task_handle *handle_c =
    gzochid_create_durable_application_task_handle
    (task_c, &string_task_serialization_c, immediate, NULL, NULL);
  
  gzochid_durable_queue *queue =
    gzochid_durable_queue_new (context->app_context);

  gzochid_durable_queue_offer
    (queue, &gzochid_durable_application_task_handle_serialization, handle_a,
     NULL);
  gzochid_durable_queue_offer
    (queue, &gzochid_durable_application_task_handle_serialization, handle_b,
     NULL);
  gzochid_durable_queue_offer
    (queue, &gzochid_durable_application_task_handle_serialization, handle_c,
     NULL);

  gzochid_schedule_durable_task_chain
    (context->app_context, context->identity, queue, NULL);

  gzochid_application_task_unref (task_a);
  gzochid_application_task_unref (task_b);
  gzochid_application_task_unref (task_c);
}

static GzochidConfiguration *
create_configuration ()
{
  GKeyFile *key_file = g_key_file_new ();
  GzochidConfiguration *configuration = NULL;

  g_key_file_set_value (key_file, "game", "lock.release.msec", "10");
  g_key_file_set_value (key_file, "game", "tx.timeout", "2147483647");
  
  configuration = g_object_new
    (GZOCHID_TYPE_CONFIGURATION, "key_file", key_file, NULL);
  
  g_key_file_unref (key_file);

  return configuration;
}

static void 
test_task_chain_simple ()
{
  test_context context;
  GzochidConfiguration *config = create_configuration ();  
  GzochidApplicationContext *app_context =
    gzochid_create_test_application_with_config (config);
  gzochid_auth_identity *identity = gzochid_auth_identity_new ("test");
  GzochidTaskQueue *task_queue = NULL;

  g_object_get (app_context, "task-queue", &task_queue, NULL);
  gzochid_schedule_task_queue_start (task_queue);
  g_object_unref (task_queue);
  
  context.app_context = app_context;
  context.identity = identity;

  g_mutex_lock (&test_worker_mutex);
  
  gzochid_wait_for_start (app_context);
  gzochid_transaction_execute (test_task_chain_inner0, &context);

  while (!test_complete)
    g_cond_wait (&test_worker_cond, &test_worker_mutex);
  g_mutex_unlock (&test_worker_mutex);

  g_assert_cmpstr (test_worker_string->str, ==, "abc");  
  g_string_free (test_worker_string, TRUE);
  
  gzochid_auth_identity_unref (identity);
  g_object_unref (app_context);

  g_object_unref (config);
}

static void
inner_main (void *data, int argc, char *argv[])
{
  g_cond_init (&test_worker_cond);
  g_mutex_init (&test_worker_mutex);

  gzochid_task_initialize_serialization_registry ();
  gzochid_task_register_serialization
    (&gzochid_task_chain_coordinator_task_serialization);
  gzochid_task_register_serialization (&string_task_serialization_a);
  gzochid_task_register_serialization (&string_task_serialization_b);
  gzochid_task_register_serialization (&string_task_serialization_c);

  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/durable-task/chain/simple", test_task_chain_simple);

  gzochid_guile_init ();
  gzochid_scheme_initialize_bindings ();
  gzochid_scheme_task_initialize_bindings ();
  exit (g_test_run ());
}

int
main (int argc, char *argv[])
{
  scm_boot_guile (argc, argv, inner_main, NULL);

  return 0;
}
