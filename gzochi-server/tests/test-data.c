/* test-data.c: Test routines for data.c in gzochid.
 * Copyright (C) 2020 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <libguile.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "app.h"
#include "app-factory.h"
#include "app-store.h"
#include "data.h"
#include "game.h"
#include "guile.h"
#include "io.h"
#include "oids.h"
#include "oids-storage.h"
#include "storage-mem.h"
#include "tx.h"

static gboolean serialized = FALSE;
static gboolean deserialized = FALSE;
static gboolean finalized = FALSE;

static gboolean
allocate_fail (gpointer data, gzochid_data_oids_block *block, GError **err)
{
  g_set_error
    (err, GZOCHID_OIDS_ERROR, GZOCHID_OIDS_ERROR_FAILED, "Allocation failure.");
  return FALSE;
}

static void 
test_serializer 
(GzochidApplicationContext *context, void *ptr, GByteArray *out, GError **err)
{
  serialized = TRUE;
}

static void 
test_failure_serializer
(GzochidApplicationContext *context, void *ptr, GByteArray *out, GError **err)
{
  g_set_error (err, GZOCHID_IO_ERROR, GZOCHID_IO_ERROR_SERIALIZATION,
	       "Failed to serialize record.");
}

static void *test_deserializer 
(GzochidApplicationContext *context, GByteArray *in, GError **err)
{
  deserialized = TRUE;
  return g_string_new_len ((const gchar *) in->data, in->len);
}

static void test_finalizer (GzochidApplicationContext *context, void *ptr)
{
  g_string_free ((GString *) ptr, TRUE);
  finalized = TRUE;
}

gzochid_io_serialization test_serialization = 
  { test_serializer, test_deserializer, test_finalizer };

gzochid_io_serialization test_serialization_failure_serializer = 
  { test_failure_serializer, test_deserializer, test_finalizer };

static void 
reset_serialization_state ()
{
  serialized = FALSE;
  deserialized = FALSE;
  finalized = FALSE;
}

static void 
fetch_reference (gpointer data)
{
  GzochidApplicationContext *context = data;
  gzochid_data_managed_reference *ref = NULL;

  ref = gzochid_data_create_reference_to_oid (context, &test_serialization, 0);
  gzochid_data_dereference (ref, NULL);
}

static void
data_reference_by_ptr_success_inner (gpointer data)
{
  GError *err = NULL;
  GString *str = g_string_new ("test-string");
  GzochidApplicationContext *context = data;
  gzochid_data_managed_reference *ref = gzochid_data_create_reference
    (context, &test_serialization, str, &err);

  g_assert (ref != NULL);
  g_assert_no_error (err); 
}

static void
test_data_reference_by_ptr_success ()
{
  GzochidApplicationContext *context = gzochid_create_test_application ();
  gzochid_application_store *app_store = NULL;
  gzochid_storage_transaction *tx = NULL;  

  g_object_get (context, "app-store", &app_store, NULL);
  
  tx = gzochid_storage_engine_interface_mem.transaction_begin
    (app_store->storage_context);
  
  gzochid_transaction_execute
    (data_reference_by_ptr_success_inner, context);
  gzochid_storage_engine_interface_mem.transaction_rollback (tx);  

  g_object_unref (context);
}

static void
data_reference_by_ptr_failure_oids_inner (gpointer data)
{
  GError *err = NULL;
  GString *str = g_string_new ("test-string");
  GzochidApplicationContext *context = data;
  gzochid_data_managed_reference *ref = gzochid_data_create_reference
    (context, &test_serialization, str, &err);

  g_assert (ref == NULL);
  g_assert_error (err, GZOCHID_DATA_ERROR, GZOCHID_DATA_ERROR_TRANSACTION);

  g_string_free (str, TRUE);
  g_error_free (err);
}

static void
test_data_reference_by_ptr_failure_oids ()
{
  GzochidApplicationContext *context = gzochid_create_test_application ();
  gzochid_application_store *app_store = NULL;
  gzochid_storage_transaction *tx = NULL;  

  g_object_get (context, "app-store", &app_store, NULL);

  /* Swap in a mock oid allocation strategy. */
  
  app_store->oid_strategy = gzochid_oid_allocation_strategy_new
    (allocate_fail, NULL, NULL);
  
  tx = gzochid_storage_engine_interface_mem.transaction_begin
    (app_store->storage_context);
  
  gzochid_transaction_execute
    (data_reference_by_ptr_failure_oids_inner, context);
  gzochid_storage_engine_interface_mem.transaction_rollback (tx);  

  gzochid_oid_allocation_strategy_free (app_store->oid_strategy);
  g_object_unref (context);
}

static void
test_data_reference_finalize ()
{
  GzochidApplicationContext *context = gzochid_create_test_application ();
  gzochid_application_store *app_store = NULL;
  gzochid_storage_transaction *tx = NULL;
  guint64 zero = 0;

  g_object_get (context, "app-store", &app_store, NULL);

  reset_serialization_state ();

  tx = gzochid_storage_engine_interface_mem.transaction_begin
    (app_store->storage_context);

  gzochid_storage_engine_interface_mem.transaction_put 
    (tx, app_store->oids, (char *) &zero, sizeof (guint64),
     (unsigned char *) "foo", 4);

  gzochid_storage_engine_interface_mem.transaction_prepare (tx);  
  gzochid_storage_engine_interface_mem.transaction_commit (tx);  
  
  gzochid_transaction_execute (fetch_reference, context);

  g_assert (!serialized);
  g_assert (deserialized);
  g_assert (finalized);

  g_object_unref (context);
}

static void
flush_reference_failure (gpointer data)
{
  GString *str = g_string_new ("test-string");
  GzochidApplicationContext *context = data;

  gzochid_data_create_reference
    (context, &test_serialization_failure_serializer, str, NULL);
}

static void
test_data_transaction_prepare_flush_reference_failure ()
{
  GzochidApplicationContext *context = gzochid_create_test_application ();
  gzochid_transaction_result result;

  reset_serialization_state ();
  result = gzochid_transaction_execute (flush_reference_failure, context);

  g_assert_cmpint (GZOCHID_TRANSACTION_SHOULD_RETRY, ==, result);

  g_assert (!serialized);
  g_assert (!deserialized);
  g_assert (finalized);  

  g_object_unref (context);
}

static void 
binding_exists_timed_out (gpointer data)
{
  GError *error = NULL;
  GzochidApplicationContext *context = data;
  gboolean exists = gzochid_data_binding_exists (context, "test", &error);

  g_assert (! exists);
  g_assert_error (error, GZOCHID_DATA_ERROR, GZOCHID_DATA_ERROR_TRANSACTION);
  g_error_free (error);
}

static 
void test_data_transaction_timeout ()
{
  struct timeval tm = { 0, 0 };
  GzochidApplicationContext *context = gzochid_create_test_application ();

  gzochid_transaction_execute_timed (binding_exists_timed_out, context, tm);

  g_object_unref (context);
}

static void
inner_main (void *data, int argc, char *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/data/reference/by-pointer/success",
		   test_data_reference_by_ptr_success);
  g_test_add_func ("/data/reference/by-pointer/failure-oids",
		   test_data_reference_by_ptr_failure_oids);
  g_test_add_func ("/data/reference/finalize", test_data_reference_finalize);
  g_test_add_func ("/data/transaction/prepare/flush-reference-failure",
		   test_data_transaction_prepare_flush_reference_failure);
  g_test_add_func ("/data/transaction/timeout", test_data_transaction_timeout);

  gzochid_guile_init ();
  exit (g_test_run ());
}

int
main (int argc, char *argv[])
{
  scm_boot_guile (argc, argv, inner_main, NULL);

  return 0;
}
