/* app-factory.h: Prototypes and declarations for app-factory.c
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GZOCHID_TEST_APP_FACTORY_H
#define GZOCHID_TEST_APP_FACTORY_H

#include "app.h"
#include "config.h"
#include "resolver.h"

/* 
   The following API supports the construction of actual 
   `GzochidApplicationContext' instances using an empty, synthetic application
   descriptor. 
   
   Use this for tests that real a real application context and
   exercise real instances of system services. (Otherwise, using `mock-app.c'.)
*/

/* Construct and return an application context using a default server 
   configuration. The returned `GzochidApplicationContext' should be released 
   via `g_object_unref' when no longer required. */

GzochidApplicationContext *gzochid_create_test_application ();

/* Construct and return an application context using the specified server 
   configuration. The returned `GzochidApplicationContext' should be released 
   via `g_object_unref' when no longer required. */

GzochidApplicationContext *gzochid_create_test_application_with_config
(GzochidConfiguration *);

/* Construct and return an application context using the specified dependency
   resolution context, which must contain a `GzochidConfiguration' instance. The
   returned `GzochidApplicationContext' should be released via `g_object_unref'
   when no longer required. */

GzochidApplicationContext *gzochid_create_test_application_with_context
(GzochidResolutionContext *);

#endif /* GZOCHID_TEST_APP_FACTORY_H */
