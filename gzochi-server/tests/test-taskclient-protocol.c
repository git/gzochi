/* test-taskclient-protocol.c: Tests for taskclient-protocol.c in gzochid.
 * Copyright (C) 2020 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <glib-object.h>
#include <stddef.h>

#include "taskclient-protocol.h"
#include "taskclient.h"

struct _GzochidTaskClient
{
  GObject parent_instance;

  GList *activity_log;
};

G_DEFINE_TYPE (GzochidTaskClient, gzochid_task_client, G_TYPE_OBJECT);

static void
task_client_finalize (GObject *gobject)
{
  GzochidTaskClient *client = GZOCHID_TASK_CLIENT (gobject);

  g_list_free_full (client->activity_log, g_free);
}

static void
gzochid_task_client_class_init (GzochidTaskClientClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = task_client_finalize;
}

static void
gzochid_task_client_init (GzochidTaskClient *self)
{
}

void
gzochid_taskclient_resubmit_tasks (GzochidTaskClient *client, const char *app,
				   GError **err)
{
  client->activity_log = g_list_append
    (client->activity_log, g_strdup_printf ("RESUBMIT %s", app));
}

void
gzochid_taskclient_application_started (GzochidTaskClient *client,
					const char *app, GError **err)
{
  client->activity_log = g_list_append
    (client->activity_log, g_strdup_printf ("START %s", app));
}

void
gzochid_taskclient_task_assigned (GzochidTaskClient *client, const char *app,
				  guint64 task_id, GError **err)
{
  client->activity_log = g_list_append
    (client->activity_log, g_strdup_printf
     ("ASSIGN %s/%" G_GUINT64_FORMAT, app, task_id));
}

void
gzochid_taskclient_task_unassigned (GzochidTaskClient *client, const char *app,
				    guint64 task_id, GError **err)
{
  client->activity_log = g_list_append
    (client->activity_log, g_strdup_printf
     ("UNASSIGN %s/%" G_GUINT64_FORMAT, app, task_id));
}

static void
test_client_can_dispatch_true ()
{
  GzochidTaskClient *client = g_object_new (GZOCHID_TYPE_TASK_CLIENT, NULL);
  GByteArray *bytes = g_byte_array_new ();

  g_byte_array_append (bytes, (const guint8 *) "\x00\x05\x81test\x00", 8);
  
  g_assert (gzochid_taskclient_client_protocol.can_dispatch (bytes, client));

  g_byte_array_unref (bytes);
  g_object_unref (client);
}

static void
test_client_can_dispatch_false ()
{
  GzochidTaskClient *client = g_object_new (GZOCHID_TYPE_TASK_CLIENT, NULL);
  GByteArray *bytes = g_byte_array_new ();

  g_byte_array_append (bytes, (const guint8 *) "\x00\x05\x81test", 7);

  g_assert
    (! gzochid_taskclient_client_protocol.can_dispatch (bytes, client));

  g_byte_array_unref (bytes);
  g_object_unref (client);
}

static void
test_client_dispatch_one_resubmit_tasks ()
{
  GzochidTaskClient *client = g_object_new (GZOCHID_TYPE_TASK_CLIENT, NULL);
  GByteArray *bytes = g_byte_array_new ();

  g_byte_array_append (bytes, (const guint8 *) "\x00\x05\x81test\x00", 8);
  
  g_assert_cmpint
    (gzochid_taskclient_client_protocol.dispatch (bytes, client), ==, 8);
  g_assert_cmpint (g_list_length (client->activity_log), ==, 1);
  g_assert_cmpstr (client->activity_log->data, ==, "RESUBMIT test");

  g_byte_array_unref (bytes);
  g_object_unref (client);
}

static void
test_client_dispatch_one_app_started ()
{
  GzochidTaskClient *client = g_object_new (GZOCHID_TYPE_TASK_CLIENT, NULL);
  GByteArray *bytes = g_byte_array_new ();

  g_byte_array_append (bytes, (const guint8 *) "\x00\x05\x83test\x00", 8);
  
  g_assert_cmpint
    (gzochid_taskclient_client_protocol.dispatch (bytes, client), ==, 8);
  g_assert_cmpint (g_list_length (client->activity_log), ==, 1);
  g_assert_cmpstr (client->activity_log->data, ==, "START test");

  g_byte_array_unref (bytes);
  g_object_unref (client);
}

static void
test_client_dispatch_one_assign_task ()
{
  GzochidTaskClient *client = g_object_new (GZOCHID_TYPE_TASK_CLIENT, NULL);
  GByteArray *bytes = g_byte_array_new ();

  g_byte_array_append
    (bytes, (const guint8 *) "\x00\x0d\x85test\x00\x00\x00\x00\x00\x00\x00"
     "\x00\x7b", 16);
  
  g_assert_cmpint
    (gzochid_taskclient_client_protocol.dispatch (bytes, client), ==, 16);
  g_assert_cmpint (g_list_length (client->activity_log), ==, 1);
  g_assert_cmpstr (client->activity_log->data, ==, "ASSIGN test/123");

  g_byte_array_unref (bytes);
  g_object_unref (client);
}

static void
test_client_dispatch_one_unassign_task ()
{
  GzochidTaskClient *client = g_object_new (GZOCHID_TYPE_TASK_CLIENT, NULL);
  GByteArray *bytes = g_byte_array_new ();

  g_byte_array_append
    (bytes, (const guint8 *) "\x00\x0d\x87test\x00\x00\x00\x00\x00\x00\x00"
     "\x00\x7b", 16);
  
  g_assert_cmpint
    (gzochid_taskclient_client_protocol.dispatch (bytes, client), ==, 16);
  g_assert_cmpint (g_list_length (client->activity_log), ==, 1);
  g_assert_cmpstr (client->activity_log->data, ==, "UNASSIGN test/123");

  g_byte_array_unref (bytes);
  g_object_unref (client);
}

static void
test_client_dispatch_multiple ()
{
  GzochidTaskClient *client = g_object_new (GZOCHID_TYPE_TASK_CLIENT, NULL);
  GByteArray *bytes = g_byte_array_new ();

  g_byte_array_append
    (bytes, (const guint8 *) "\x00\x0d\x85test\x00\x00\x00\x00\x00\x00\x00"
     "\x00\x7b", 16);
  g_byte_array_append
    (bytes, (const guint8 *) "\x00\x0d\x87test\x00\x00\x00\x00\x00\x00\x00"
     "\x00\x7c", 16);

  g_assert_cmpint
    (gzochid_taskclient_client_protocol.dispatch (bytes, client), ==, 32);
  g_assert_cmpint (g_list_length (client->activity_log), ==, 2);
  g_assert_cmpstr (client->activity_log->data, ==, "ASSIGN test/123");
  g_assert_cmpstr (client->activity_log->next->data, ==, "UNASSIGN test/124");
  
  g_byte_array_unref (bytes);
  g_object_unref (client);
}

int
main (int argc, char *argv[])
{  
#if GLIB_CHECK_VERSION (2, 36, 0)
  /* No need for `g_type_init'. */
#else
  g_type_init ();
#endif /* GLIB_CHECK_VERSION */

  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/client/can_dispatch/true", test_client_can_dispatch_true);
  g_test_add_func
    ("/client/can_dispatch/false", test_client_can_dispatch_false);

  g_test_add_func ("/client/dispatch/one/resubmit-tasks",
		   test_client_dispatch_one_resubmit_tasks);
  g_test_add_func ("/client/dispatch/one/app-started",
		   test_client_dispatch_one_app_started);
  g_test_add_func ("/client/dispatch/one/assign-task",
		   test_client_dispatch_one_assign_task);
  g_test_add_func ("/client/dispatch/one/unassign-task",
		   test_client_dispatch_one_unassign_task);

  g_test_add_func ("/client/dispatch/multiple", test_client_dispatch_multiple);
  
  return g_test_run ();
}
