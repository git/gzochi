/* test-taskclient.c: Tests for taskclient.c in gzochid.
 * Copyright (C) 2020 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <glib-object.h>
#include <gzochi-common.h>
#include <stddef.h>
#include <stdlib.h>

#include "app-task.h"
#include "auth_int.h"
#include "data.h"
#include "descriptor.h"
#include "durable-task.h"
#include "event.h"
#include "game.h"
#include "mock-data.h"
#include "schedule.h"
#include "socket.h"
#include "taskclient.h"
#include "util.h"

static GPrivate name_key = G_PRIVATE_INIT (free);

void
gzochid_mock_application_install_properties (GObjectClass *object_class)
{
  g_object_class_install_property
    (object_class, 1, g_param_spec_pointer
     ("app-state", "app-state", "app-state", G_PARAM_READWRITE));
  g_object_class_install_property
    (object_class, 2, g_param_spec_object
     ("descriptor", "descriptor", "descriptor", G_TYPE_OBJECT,
      G_PARAM_READWRITE));
  g_object_class_install_property
    (object_class, 3, g_param_spec_boxed
     ("event-source", "event-source", "event-source", G_TYPE_SOURCE,
      G_PARAM_READWRITE));
  g_object_class_install_property
    (object_class, 4, g_param_spec_object
     ("metaclient", "metaclient", "metaclient", G_TYPE_OBJECT,
      G_PARAM_READWRITE));
  g_object_class_install_property
    (object_class, 5, g_param_spec_pointer
     ("name", "name", "name", G_PARAM_READWRITE));
  g_object_class_install_property
    (object_class, 6, g_param_spec_object 
     ("task-queue", "task-queue", "task-queue", G_TYPE_OBJECT,
      G_PARAM_READWRITE));
  g_object_class_install_property
    (object_class, 7, g_param_spec_uint64
     ("tx-timeout", "tx-timeout", "tx-timeout", 0, G_MAXUINT64, G_MAXUINT64,
      G_PARAM_READWRITE));
}

struct _GzochidGameServer
{
  GObject parent_instance;

  GHashTable *applications;
};

G_DEFINE_TYPE (GzochidGameServer, gzochid_game_server, G_TYPE_OBJECT);

static void
game_server_finalize (GObject *obj)
{
  GzochidGameServer *self = GZOCHID_GAME_SERVER (obj);

  g_hash_table_destroy (self->applications);

  G_OBJECT_CLASS (gzochid_game_server_parent_class)->finalize (obj);
}

static void
gzochid_game_server_class_init (GzochidGameServerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = game_server_finalize;
}

static void
gzochid_game_server_init (GzochidGameServer *self)
{
  self->applications = g_hash_table_new (g_str_hash, g_str_equal);
}

GzochidApplicationContext *
gzochid_game_server_lookup_application (GzochidGameServer *game_server,
					const char *app)
{
  return g_hash_table_lookup (game_server->applications, app);
}

struct _GzochidTaskQueue
{
  GObject parent_instance;

  GHashTable *tasks;

  GList *task_threads;
  GMutex task_thread_mutex;
};

G_DEFINE_TYPE (GzochidTaskQueue, gzochid_task_queue, G_TYPE_OBJECT);

gpointer
task_executor_wrapper (gpointer data)
{
  GzochidTask *task = data;
  task->worker (task->data, NULL);

  g_object_unref (task);
  
  return NULL;
}

static void
task_queue_finalize (GObject *obj)
{
  GzochidTaskQueue *self = GZOCHID_TASK_QUEUE (obj);

  g_hash_table_destroy (self->tasks);
  
  g_mutex_clear (&self->task_thread_mutex);  
  G_OBJECT_CLASS (gzochid_task_queue_parent_class)->finalize (obj);
}

static void
gzochid_task_queue_class_init (GzochidTaskQueueClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = task_queue_finalize;
}

static void
gzochid_task_queue_init (GzochidTaskQueue *self)
{
  self->tasks = g_hash_table_new_full
    (g_int64_hash, g_int64_equal, free, (GDestroyNotify) g_object_unref);

  g_mutex_init (&self->task_thread_mutex);
}

struct _GzochidMetaClient
{
  GObject parent_instance;

  GzochidTaskClient *taskclient;
};

G_DEFINE_TYPE (GzochidMetaClient, gzochid_meta_client, G_TYPE_OBJECT);

static void
meta_client_dispose (GObject *object)
{
  GzochidMetaClient *client = GZOCHID_META_CLIENT (object);
  g_object_unref (client->taskclient);
  G_OBJECT_CLASS (gzochid_meta_client_parent_class)->dispose (object);
}

static void
meta_client_get_property (GObject *object, guint property_id, GValue *value,
			  GParamSpec *pspec)
{
  GzochidMetaClient *self = GZOCHID_META_CLIENT (object);

  switch (property_id)
    {
    case 1:
      g_value_set_object (value, self->taskclient);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }  
}

static void
meta_client_set_property (GObject *object, guint property_id,
			  const GValue *value, GParamSpec *pspec)
{
  GzochidMetaClient *self = GZOCHID_META_CLIENT (object);

  switch (property_id)
    {
    case 1:
      self->taskclient = g_object_ref (g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }  
}

static void
gzochid_meta_client_class_init (GzochidMetaClientClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = meta_client_dispose;
  object_class->get_property = meta_client_get_property;
  object_class->set_property = meta_client_set_property;

  g_object_class_install_property
    (object_class, 1, g_param_spec_object
     ("task-client", "task-client", "task-client", GZOCHID_TYPE_TASK_CLIENT,
      G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
}

static void
gzochid_meta_client_init (GzochidMetaClient *self)
{
}

struct _taskclient_fixture
{
  GzochidApplicationDescriptor *descriptor;
  GzochidGameServer *game_server;
  gzochid_reconnectable_socket *socket;
  GzochidMetaClient *metaclient;
  GzochidTaskClient *taskclient;
  GzochidTaskQueue *task_queue;
  GzochidApplicationContext *app_context;
};

typedef struct _taskclient_fixture taskclient_fixture;

struct _gzochid_reconnectable_socket
{
  GByteArray *bytes_received;
};

gzochid_reconnectable_socket *
gzochid_reconnectable_socket_new ()
{
  gzochid_reconnectable_socket *sock =
    malloc (sizeof (gzochid_reconnectable_socket));

  sock->bytes_received = g_byte_array_new ();
  
  return sock;
}

void
gzochid_reconnectable_socket_free (gzochid_reconnectable_socket *sock)
{
  g_byte_array_unref (sock->bytes_received);
  free (sock);
}

void
gzochid_reconnectable_socket_write (gzochid_reconnectable_socket *sock,
				    unsigned char *buf, size_t len)
{
  g_byte_array_append (sock->bytes_received, buf, len);
}

guint64
gzochid_schedule_submit_task (GzochidTaskQueue *task_queue, GzochidTask *task)
{
  static guint64 task_id = 0;
  G_LOCK_DEFINE_STATIC (task_id);

  G_LOCK (task_id);
  task_id++;
  G_UNLOCK (task_id);

  if (g_private_get (&name_key) != NULL)
    {
      g_mutex_lock (&task_queue->task_thread_mutex);
      task_queue->task_threads = g_list_append
	(task_queue->task_threads,
	 g_thread_new ("test-submit", task_executor_wrapper, task));
      g_mutex_unlock (&task_queue->task_thread_mutex);
    }
  else g_hash_table_insert (task_queue->tasks,
			    g_memdup (&task_id, sizeof (guint64)),
			    g_object_ref_sink (task));
  
  return task_id;
}

gboolean
gzochid_schedule_remove_task (GzochidTaskQueue *task_queue,
			      guint64 local_task_id)
{
  return g_hash_table_remove (task_queue->tasks, &local_task_id);
}

void
gzochid_scheme_application_initialized_worker
(GzochidApplicationContext *app_context, gzochid_auth_identity *identity,
 gpointer data)
{
}

static void
taskclient_fixture_setup (taskclient_fixture *fixture, gconstpointer user_data)
{
  guint64 tx_timeout_ms = 10000;
  gzochid_event_source *event_source = gzochid_event_source_new ();

  fixture->descriptor = g_object_new
    (GZOCHID_TYPE_APPLICATION_DESCRIPTOR, NULL);
  fixture->game_server = g_object_new (GZOCHID_TYPE_GAME_SERVER, NULL);
  fixture->socket = gzochid_reconnectable_socket_new ();
  fixture->task_queue = g_object_new (GZOCHID_TYPE_TASK_QUEUE, NULL);
  fixture->taskclient = g_object_new
    (GZOCHID_TYPE_TASK_CLIENT,
     "game-server", fixture->game_server,
     "task-queue", fixture->task_queue,
     "reconnectable-socket", fixture->socket,
     NULL);
  fixture->metaclient = g_object_new
    (GZOCHID_TYPE_META_CLIENT, "task-client", fixture->taskclient, NULL);

  fixture->app_context = g_object_new
    (GZOCHID_TYPE_APPLICATION_CONTEXT,
     "app-state", gzochid_application_state_new (),
     "descriptor", fixture->descriptor,
     "event-source", event_source,
     "metaclient", fixture->metaclient,
     "name", "test",
     "task-queue", fixture->task_queue,
     "tx-timeout", &tx_timeout_ms,
     NULL);

  g_hash_table_insert
    (fixture->game_server->applications, "test", fixture->app_context);

  gzochid_data_set_binding_to_oid
    (fixture->app_context, "s.initializer", 1, NULL);
  
  g_source_unref ((GSource *) event_source);
}

static void
wait_for_threads (taskclient_fixture *fixture)
{
  g_mutex_lock (&fixture->task_queue->task_thread_mutex);
  g_list_free_full
    (fixture->task_queue->task_threads, (GDestroyNotify) g_thread_join);
  fixture->task_queue->task_threads = NULL;
  g_mutex_unlock (&fixture->task_queue->task_thread_mutex);  
}

static void
wait_for_tasks (taskclient_fixture *fixture)
{
  while (g_hash_table_size (fixture->task_queue->tasks) > 0)
    {
      GHashTableIter iter;
      gpointer key, value;

      g_hash_table_iter_init (&iter, fixture->task_queue->tasks);

      while (g_hash_table_iter_next (&iter, &key, &value))
	{
	  GzochidTask *task = value;
	  task->worker (task->data, NULL);      
	  g_hash_table_iter_remove (&iter);
	}
    }
}

static void
taskclient_fixture_teardown (taskclient_fixture *fixture,
			     gconstpointer user_data)
{
  gzochid_application_state *state = NULL;

  g_object_get (fixture->app_context, "app-state", &state, NULL);
  gzochid_application_state_free (state);

  g_object_unref (fixture->descriptor);
  g_object_unref (fixture->game_server);
  gzochid_reconnectable_socket_free (fixture->socket);
  g_object_unref (fixture->task_queue);
  g_object_unref (fixture->metaclient);
  g_object_unref (fixture->taskclient);
  g_object_unref (fixture->app_context);

  gzochid_test_mock_data_clear ();
}

static void
test_application_started (taskclient_fixture *fixture, gconstpointer user_data)
{
  GError *err = NULL;
  gzochid_application_state *state = NULL;

  g_object_get (fixture->app_context, "app-state", &state, NULL);

  g_private_set (&name_key, "assigning");

  gzochid_taskclient_application_started (fixture->taskclient, "test", &err);

  g_assert_no_error (err);
  wait_for_threads (fixture);
  wait_for_tasks (fixture);

  g_private_set (&name_key, NULL);

  g_assert_cmpint (state->bootstrap_stage, ==, STARTED);
}

static void
test_cancel_task (taskclient_fixture *fixture, gconstpointer user_data)
{
  GBytes *actual = NULL;
  GBytes *expected = g_bytes_new_static
    ("\x00\x0d\x88test\x00\x00\x00\x00\x00\x00\x00\x00\x7b", 16);

  gzochid_start_task_processing (fixture->app_context);
  gzochid_taskclient_cancel_task (fixture->taskclient, "test", 123);

  actual = g_bytes_new_static
    (fixture->socket->bytes_received->data,
     fixture->socket->bytes_received->len);

  g_assert (g_bytes_equal (expected, actual));
  
  g_bytes_unref (actual);
  g_bytes_unref (expected);
}

static void
test_application_worker (GzochidApplicationContext *context,
			 gzochid_auth_identity *identity, gpointer data)
{
}

static void
test_worker_serializer (GzochidApplicationContext *context,
			gzochid_application_worker worker, GByteArray *arr)
{
}

static gzochid_application_worker
test_worker_deserializer (GzochidApplicationContext *context, GByteArray *arr)
{
  return test_application_worker;
}

static gzochid_application_worker_serialization test_worker_serialization =
  { test_worker_serializer, test_worker_deserializer };

static void
test_null_serializer (GzochidApplicationContext *context, void *data,
		      GByteArray *arr, GError **err)
{
}

static void *
test_null_deserializer (GzochidApplicationContext *context, GByteArray *arr,
			GError **err)
{
  return NULL;
}

static void
test_null_finalizer (GzochidApplicationContext *context, void *data)
{
}

static gzochid_io_serialization test_serialization =
  { test_null_serializer, test_null_deserializer, test_null_finalizer };

static gzochid_application_task_serialization test_task_serialization =
  { "test", &test_worker_serialization, &test_serialization };

static guint64
stub_task_handle (taskclient_fixture *fixture)
{
  gzochid_application_task *task = gzochid_application_task_new
    (fixture->app_context, gzochid_auth_system_identity (),
     test_application_worker, NULL, fixture);
  struct timeval delay = { 0, 0 };
  gzochid_durable_application_task_handle *task_handle =
    gzochid_create_durable_application_task_handle
    (task, &test_task_serialization, delay, NULL, NULL);
  
  gzochid_data_managed_reference *reference = gzochid_data_create_reference
    (fixture->app_context,
     &gzochid_durable_application_task_handle_serialization, task_handle, NULL);

  gzochid_application_task_unref (task);
  
  return reference->oid;
}

static void
test_complete_task (taskclient_fixture *fixture, gconstpointer user_data)
{
  GError *err = NULL;
  GBytes *actual = NULL, *expected = NULL;
  GByteArray *expected_arr = g_byte_array_sized_new (16);
  guint64 task_id = stub_task_handle (fixture);

  g_private_set (&name_key, "assigning");

  gzochid_start_task_processing (fixture->app_context);
  wait_for_threads (fixture);
  wait_for_tasks (fixture);
  
  g_private_set (&name_key, NULL);
  
  g_byte_array_append
    (expected_arr, (const guint8 *) "\x00\x0d\x86test\x00", 8);
  gzochi_common_io_write_long (task_id, expected_arr->data, 8);
  g_byte_array_set_size (expected_arr, 16);
  
  g_private_set (&name_key, "assigning");
  
  gzochid_taskclient_task_assigned (fixture->taskclient, "test", task_id, NULL);
  wait_for_threads (fixture);
  gzochid_taskclient_complete_task (fixture->taskclient, "test", task_id, &err);

  g_private_set (&name_key, NULL);
  
  g_assert_no_error (err);

  actual = g_bytes_new_static
    (fixture->socket->bytes_received->data,
     fixture->socket->bytes_received->len);
  expected = g_byte_array_free_to_bytes (expected_arr);
  
  g_assert (g_bytes_equal (expected, actual));
  
  g_bytes_unref (actual);
  g_bytes_unref (expected);
}

static void
test_resubmit_tasks (taskclient_fixture *fixture, gconstpointer user_data)
{
  GError *err = NULL;
  GBytes *actual = NULL, *expected = NULL;
  GByteArray *expected_arr = g_byte_array_sized_new (16);
  guint64 task_id = stub_task_handle (fixture);
  gchar *binding_str = g_strdup_printf
    ("s.pendingTask.%" G_GUINT64_FORMAT, task_id);
  GList *tasks = NULL, *task_ptr = NULL;
  
  gzochid_data_set_binding_to_oid
    (fixture->app_context, binding_str, task_id, NULL);
  
  g_byte_array_append
    (expected_arr, (const guint8 *) "\x00\x0d\x84test\x00", 8);
  gzochi_common_io_write_long (task_id, expected_arr->data, 8);
  g_byte_array_set_size (expected_arr, 16);
  g_byte_array_append
    (expected_arr, (const guint8 *) "\x00\x05\x82test\x00", 8);

  g_private_set (&name_key, "assigning");

  gzochid_taskclient_resubmit_tasks (fixture->taskclient, "test", &err);
  wait_for_threads (fixture);

  g_private_set (&name_key, NULL);
  wait_for_tasks (fixture);
  
  g_assert_no_error (err);

  actual = g_bytes_new_static
    (fixture->socket->bytes_received->data,
     MIN (fixture->socket->bytes_received->len, 24));
  expected = g_byte_array_free_to_bytes (expected_arr);
  
  g_assert (g_bytes_equal (expected, actual));
  
  g_bytes_unref (actual);
  g_bytes_unref (expected);

  g_free (binding_str);
}

static void
test_start_application (taskclient_fixture *fixture, gconstpointer user_data)
{
  GBytes *actual = NULL;
  GBytes *expected = g_bytes_new_static ("\x00\x05\x80test\x00", 8);

  gzochid_taskclient_start_application (fixture->taskclient, "test");
  
  actual = g_bytes_new_static
    (fixture->socket->bytes_received->data,
     fixture->socket->bytes_received->len);

  g_assert (g_bytes_equal (expected, actual));
  
  g_bytes_unref (actual);
  g_bytes_unref (expected);
}

static void
test_submit_assigned_task (taskclient_fixture *fixture, gconstpointer user_data)
{
  GError *err = NULL;
  guint64 task_id = stub_task_handle (fixture);

  g_private_set (&name_key, "assigning");

  gzochid_start_task_processing (fixture->app_context);
  wait_for_threads (fixture);
  wait_for_tasks (fixture);

  gzochid_taskclient_task_assigned (fixture->taskclient, "test", task_id, NULL);
  wait_for_threads (fixture);

  g_private_set (&name_key, NULL);

  gzochid_taskclient_submit_task
    (fixture->taskclient, "test", task_id, 123, NULL);

  g_assert_no_error (err);
  g_assert_cmpint (fixture->socket->bytes_received->len, ==, 0);
}

static void
test_submit_new_task (taskclient_fixture *fixture, gconstpointer user_data)
{
  GError *err = NULL;
  GBytes *actual = NULL;
  GBytes *expected = g_bytes_new_static
    ("\x00\x0d\x84test\x00\x00\x00\x00\x00\x00\x00\x00\x7b", 16);
  
  gzochid_start_task_processing (fixture->app_context);
  gzochid_taskclient_submit_task (fixture->taskclient, "test", 123, 456, &err);

  g_assert_no_error (err);

  actual = g_bytes_new_static
    (fixture->socket->bytes_received->data,
     fixture->socket->bytes_received->len);

  g_assert (g_bytes_equal (expected, actual));
  
  g_bytes_unref (actual);
  g_bytes_unref (expected);
}

static void
test_task_assigned (taskclient_fixture *fixture, gconstpointer user_data)
{
  GError *err = NULL;
  guint64 task_id = stub_task_handle (fixture);

  g_private_set (&name_key, "assigning");

  gzochid_start_task_processing (fixture->app_context);
  wait_for_threads (fixture);
  wait_for_tasks (fixture);  
  
  gzochid_taskclient_task_assigned (fixture->taskclient, "test", task_id, &err);
  wait_for_threads (fixture);
  
  g_private_set (&name_key, NULL);

  g_assert_no_error (err);
  g_assert (g_hash_table_size (fixture->task_queue->tasks) > 0);
}

static void
test_task_unassigned (taskclient_fixture *fixture, gconstpointer user_data)
{
  GError *err = NULL;
  guint64 task_id = stub_task_handle (fixture);

  g_private_set (&name_key, "assigning");

  gzochid_start_task_processing (fixture->app_context);
  wait_for_threads (fixture);
  wait_for_tasks (fixture);  
  
  gzochid_taskclient_task_assigned (fixture->taskclient, "test", task_id, NULL);
  wait_for_threads (fixture);
  
  g_private_set (&name_key, NULL);  

  gzochid_taskclient_task_unassigned
    (fixture->taskclient, "test", task_id, &err);
  g_assert_no_error (err);

  gzochid_taskclient_task_unassigned
    (fixture->taskclient, "test", task_id, &err);
  g_assert_error (err, GZOCHID_TASKCLIENT_ERROR,
		  GZOCHID_TASKCLIENT_ERROR_TASK_NOT_MAPPED);

  g_error_free (err);
}

int
main (int argc, char *argv[])
{  
#if GLIB_CHECK_VERSION (2, 36, 0)
  /* No need for `g_type_init'. */
#else
  g_type_init ();
#endif /* GLIB_CHECK_VERSION */

  g_test_init (&argc, &argv, NULL);
  
  g_test_add
    ("/taskclient/application-started", taskclient_fixture, NULL,
     taskclient_fixture_setup, test_application_started,
     taskclient_fixture_teardown);
  g_test_add
    ("/taskclient/cancel-task", taskclient_fixture, NULL,
     taskclient_fixture_setup, test_cancel_task, taskclient_fixture_teardown);
  g_test_add
    ("/taskclient/complete-task", taskclient_fixture, NULL,
     taskclient_fixture_setup, test_complete_task, taskclient_fixture_teardown);
  g_test_add
    ("/taskclient/resubmit-tasks", taskclient_fixture, NULL,
     taskclient_fixture_setup, test_resubmit_tasks,
     taskclient_fixture_teardown);
  g_test_add
    ("/taskclient/start-application", taskclient_fixture, NULL,
     taskclient_fixture_setup, test_start_application,
     taskclient_fixture_teardown);
  g_test_add
    ("/taskclient/submit-task/assigned", taskclient_fixture, NULL,
     taskclient_fixture_setup, test_submit_assigned_task,
     taskclient_fixture_teardown);
  g_test_add
    ("/taskclient/submit-task/new", taskclient_fixture, NULL,
     taskclient_fixture_setup, test_submit_new_task,
     taskclient_fixture_teardown);
  g_test_add
    ("/taskclient/task-assigned", taskclient_fixture, NULL,
     taskclient_fixture_setup, test_task_assigned, taskclient_fixture_teardown);
  g_test_add
    ("/taskclient/task-unassigned", taskclient_fixture, NULL,
     taskclient_fixture_setup, test_task_unassigned,
     taskclient_fixture_teardown);

  gzochid_task_initialize_serialization_registry ();
  gzochid_task_register_serialization (&test_task_serialization);

  gzochid_test_mock_data_initialize ();

  return g_test_run ();
}
