/* test-stats.c: Test routines for stats.c in gzochid.
 * Copyright (C) 2018 Julian Graham
 *
 * gzochi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <stddef.h>

#include "event.h"
#include "stats.h"

static void
test_stats_update_bytes_read (GzochidApplicationStats *stats,
			      gconstpointer user_data)
{
  GzochidDataEvent *event = g_object_new
    (GZOCHID_TYPE_DATA_EVENT, "type", BYTES_READ, "bytes", 123, NULL);

  gzochid_stats_update_from_event (stats, GZOCHID_EVENT (event));

  g_assert_cmpint (stats->bytes_read, ==, 123);
  
  g_object_unref (event);
}

static void
test_stats_update_bytes_written (GzochidApplicationStats *stats,
				 gconstpointer user_data)
{
  GzochidDataEvent *event = g_object_new
    (GZOCHID_TYPE_DATA_EVENT, "type", BYTES_WRITTEN, "bytes", 123, NULL);

  gzochid_stats_update_from_event (stats, GZOCHID_EVENT (event));

  g_assert_cmpint (stats->bytes_written, ==, 123);

  g_object_unref (event);
}

static void
test_stats_update_messages_received (GzochidApplicationStats *stats,
				     gconstpointer user_data)
{
  GzochidEvent *event = g_object_new
    (GZOCHID_TYPE_EVENT, "type", MESSAGE_RECEIVED, NULL);

  gzochid_stats_update_from_event (stats, event);

  g_assert_cmpint (stats->num_messages_received, ==, 1);
  
  g_object_unref (event);
}

static void
test_stats_update_messages_sent (GzochidApplicationStats *stats,
				 gconstpointer user_data)
{
  GzochidEvent *event = g_object_new
    (GZOCHID_TYPE_EVENT, "type", MESSAGE_SENT, NULL);

  gzochid_stats_update_from_event (stats, event);

  g_assert_cmpint (stats->num_messages_sent, ==, 1);

  g_object_unref (event);
}

static void
test_stats_update_tx_started (GzochidApplicationStats *stats,
			      gconstpointer user_data)
{
  GzochidEvent *event = g_object_new
    (GZOCHID_TYPE_EVENT, "type", TRANSACTION_START, NULL);

  gzochid_stats_update_from_event (stats, event);

  g_assert_cmpint (stats->num_transactions_started, ==, 1);
  
  g_object_unref (event);
}

static void
test_stats_update_tx_committed (GzochidApplicationStats *stats,
				gconstpointer user_data)
{
  GzochidTransactionEvent *event = g_object_new
    (GZOCHID_TYPE_TRANSACTION_EVENT,
     "type", TRANSACTION_COMMIT, "duration-us", 10000, NULL);

  gzochid_stats_update_from_event (stats, GZOCHID_EVENT (event));

  g_assert_cmpint (stats->num_transactions_committed, ==, 1);
  g_assert_cmpint (stats->min_transaction_duration, ==, 10);
  g_assert_cmpint (stats->max_transaction_duration, ==, 10);
  g_assert_cmpint (stats->average_transaction_duration, ==, 10);
  
  g_object_unref (event);
}

static void
test_stats_update_tx_rolled_back (GzochidApplicationStats *stats,
				  gconstpointer user_data)
{
  GzochidTransactionEvent *event = g_object_new
    (GZOCHID_TYPE_TRANSACTION_EVENT,
     "type", TRANSACTION_ROLLBACK, "duration-us", 10000, NULL);

  gzochid_stats_update_from_event (stats, GZOCHID_EVENT (event));

  g_assert_cmpint (stats->num_transactions_rolled_back, ==, 1);
  g_assert_cmpint (stats->min_transaction_duration, ==, 0);
  g_assert_cmpint (stats->max_transaction_duration, ==, 0);
  g_assert_cmpint (stats->average_transaction_duration, ==, 0);

  g_object_unref (event);
}

static void
test_stats_update_max_tx_duration (GzochidApplicationStats *stats,
				   gconstpointer user_data)
{
  GzochidTransactionEvent *event1 = g_object_new
    (GZOCHID_TYPE_TRANSACTION_EVENT,
     "type", TRANSACTION_COMMIT, "duration-us", 10000, NULL);
  GzochidTransactionEvent *event2 = g_object_new
    (GZOCHID_TYPE_TRANSACTION_EVENT,
     "type", TRANSACTION_COMMIT, "duration-us", 20000, NULL);

  gzochid_stats_update_from_event (stats, GZOCHID_EVENT (event1));
  gzochid_stats_update_from_event (stats, GZOCHID_EVENT (event2));

  g_assert_cmpint (stats->min_transaction_duration, ==, 10);
  g_assert_cmpint (stats->max_transaction_duration, ==, 20);
  g_assert_cmpint (stats->average_transaction_duration, ==, 15);

  g_object_unref (event1);
  g_object_unref (event2);
}

static void
test_stats_update_min_tx_duration (GzochidApplicationStats *stats,
				   gconstpointer user_data)
{
  GzochidTransactionEvent *event1 = g_object_new
    (GZOCHID_TYPE_TRANSACTION_EVENT,
     "type", TRANSACTION_COMMIT, "duration-us", 10000, NULL);
  GzochidTransactionEvent *event2 = g_object_new
    (GZOCHID_TYPE_TRANSACTION_EVENT,
     "type", TRANSACTION_COMMIT, "duration-us", 6000, NULL);

  gzochid_stats_update_from_event (stats, GZOCHID_EVENT (event1));
  gzochid_stats_update_from_event (stats, GZOCHID_EVENT (event2));

  g_assert_cmpint (stats->min_transaction_duration, ==, 6);
  g_assert_cmpint (stats->max_transaction_duration, ==, 10);
  g_assert_cmpint (stats->average_transaction_duration, ==, 8);

  g_object_unref (event1);
  g_object_unref (event2);
}

int
main (int argc, char *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add ("/stats/update/bytes-read", GzochidApplicationStats, NULL, NULL,
	      test_stats_update_bytes_read, NULL);
  g_test_add ("/stats/update/bytes-written", GzochidApplicationStats, NULL,
	      NULL, test_stats_update_bytes_written, NULL);
  g_test_add ("/stats/update/messages-received", GzochidApplicationStats, NULL,
	      NULL, test_stats_update_messages_received, NULL);
  g_test_add ("/stats/update/messages-sent", GzochidApplicationStats, NULL,
	      NULL, test_stats_update_messages_sent, NULL);
  g_test_add ("/stats/update/tx-started", GzochidApplicationStats, NULL, NULL,
	      test_stats_update_tx_started, NULL);
  g_test_add ("/stats/update/tx-committed", GzochidApplicationStats, NULL, NULL,
	      test_stats_update_tx_committed, NULL);
  g_test_add ("/stats/update/tx-rolled-back", GzochidApplicationStats, NULL,
	      NULL, test_stats_update_tx_rolled_back, NULL);
  g_test_add ("/stats/update/max-tx-duration", GzochidApplicationStats, NULL,
	      NULL, test_stats_update_max_tx_duration, NULL);
  g_test_add ("/stats/update/min-tx-duration", GzochidApplicationStats, NULL,
	      NULL, test_stats_update_min_tx_duration, NULL);
  
  return g_test_run ();
}
