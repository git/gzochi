;; gzochi/test-module.scm: Module of fixtures for gzochi API tests
;; Copyright (C) 2018 Julian Graham
;;
;; gzochi is free software: you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(library (gzochi test-module)
  (export test-callback
	  test-serialization
	  test-thunk-callback
	  test-unary-callback

	  thunk-procedure
	  unary-procedure)
  (import (gzochi app)
	  (gzochi data)
	  (gzochi io)
	  (gzochi mock-data)
	  (rnrs base))

  (initialize-mock-data)
  
  ;; N.B. There are no actual _tests_ defined in this module. Instead, it's a
  ;; convenient home for definitions used by actual test code that verifies
  ;; that functions can be resolved across module boundaries.
  
  (define test-callback (g:@ gzochi:write-integer))

  (define test-serialization (g:<-> gzochi:write-integer gzochi:read-integer))

  (define (thunk-procedure) #t)
  
  (define test-thunk-callback (g:@ thunk-procedure))

  (define (unary-procedure arg) arg)

  (define test-unary-callback
    (let ((val (gzochi:make-managed-serializable 123 test-serialization)))
      (g:@ unary-procedure val)))
)
