;; gzochi/private/test-task.scm: Scheme unit tests for private task module
;; Copyright (C) 2018 Julian Graham
;;
;; gzochi is free software: you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!r6rs

(import (gzochi data))
(import (gzochi private task))
(import (gzochi srfi-64-support))
(import (gzochi test-module))
(import (rnrs))
(import (srfi :64))

(test-runner-current (gzochi:test-runner))

(test-begin "gzochi:run-task")

(test-group "thunk" (test-assert (gzochi:run-task test-thunk-callback)))
(test-group "with-argument"
  (test-eqv 123 (gzochi:managed-serializable-value
		 (gzochi:run-task test-unary-callback))))

(test-end "gzochi:run-task")
