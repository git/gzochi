;; gzochi/test-io.scm: Scheme unit tests for gzochi io API
;; Copyright (C) 2018 Julian Graham
;;
;; gzochi is free software: you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!r6rs

(library (gzochi test-io)
  (export write-foo read-foo)
  (import (gzochi io)
	  (gzochi srfi-64-support)
	  (gzochi test-module)
	  (rnrs)
	  (srfi :64))

(define (write-foo) (if #f #f))
(define (read-foo) (if #f #f))

(test-runner-current (gzochi:test-runner))

(test-begin "g:<->")

(test-group "implicit-module"
  (let ((ser (g:<-> gzochi:write-string gzochi:read-string)))
    (test-equal 'gzochi:write-string
		(gzochi:serialization-reference-serializer-procedure ser))
    (test-equal '(gzochi io)
		(gzochi:serialization-reference-serializer-module-name ser))
    (test-equal 'gzochi:read-string
		(gzochi:serialization-reference-deserializer-procedure ser))
    (test-equal '(gzochi io)
		(gzochi:serialization-reference-serializer-module-name ser)))

  (let ((ser (g:<-> write-foo read-foo)))
    (test-equal 'write-foo
		(gzochi:serialization-reference-serializer-procedure ser))
    (test-equal '(gzochi test-io)
		(gzochi:serialization-reference-serializer-module-name ser))
    (test-equal 'read-foo
		(gzochi:serialization-reference-deserializer-procedure ser))
    (test-equal '(gzochi test-io)
		(gzochi:serialization-reference-serializer-module-name ser)))

  (test-equal 'gzochi:write-integer
	      (gzochi:serialization-reference-serializer-procedure
	       test-serialization))
  (test-equal '(gzochi io)
	      (gzochi:serialization-reference-serializer-module-name
	       test-serialization))
  (test-equal 'gzochi:read-integer
	      (gzochi:serialization-reference-deserializer-procedure
	       test-serialization))
  (test-equal '(gzochi io)
	      (gzochi:serialization-reference-serializer-module-name
	       test-serialization)))

(test-end "g:<->")

(test-begin "gzochi:make-uniform-vector-serialization")

(define serialized-vector #vu8(5 3 0 3 102 111 111 2 3 98 97 114 4 3 98 97 122))
(define test-vector (vector "foo" #f "bar" #f "baz"))

(test-group "serialize"
  (let* ((serialization (gzochi:make-uniform-vector-serialization
			 gzochi:string-serialization)))

    (call-with-values (lambda () (open-bytevector-output-port))
      (lambda (port get-bytevector)
	((gzochi:serialization-serializer serialization) port test-vector)
	(test-assert (bytevector=? (get-bytevector) serialized-vector))))))

(test-group "deserialize"
  (let* ((serialization (gzochi:make-uniform-vector-serialization
			 gzochi:string-serialization)))

    (test-equal ((gzochi:serialization-deserializer serialization)
		 (open-bytevector-input-port serialized-vector))
		test-vector)))

(test-group "fill"
  (let* ((serialization (gzochi:make-uniform-vector-serialization
			 gzochi:string-serialization #\|)))

    (test-equal ((gzochi:serialization-deserializer serialization)
		 (open-bytevector-input-port serialized-vector))
		(vector "foo" #\| "bar" #\| "baz"))))

(test-end "gzochi:make-uniform-vector-serialization")
)
